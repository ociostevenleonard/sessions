/*Creating normal object using variable*/
let cellphone = {
	name: "Nokia 3210",
	manufactureDate: 1999
};	

console.log("Result from creating an object: ");
console.log(cellphone);
console.log(typeof cellphone);

/*Creating Object using a constructor function*/

function LapTop(brand, name, manufactureDate){
	this.Brand_InsideLaptop = brand,
	this.Name_InsideLaptop = name,
	this.ManuDate_InsideLaptop = manufactureDate;
}

let LaptopHolder = new LapTop("Dell", "Steven's Laptop", "08/08/2020");

console.log("Result From INstance: ");
console.log(LaptopHolder);
console.log("Result From Keys Inside INstance: ");
console.log(LaptopHolder.ManuDate_InsideLaptop);


/*Empty Object*/
let car = {};

/*Adding property*/

/*dot notation*/
car.name = "honda civic";
console.log(car);

/*bracket notation*/
car['manufactureDate'] = 2019;
console.log(car['manufactureDate']);
console.log(car.manufactureDate);

console.log(car);

/*Object Methods*/
let person = {
	name: "Steven",
	talk: function(){
		console.log("Hello my name is " + this.name); /*function inside an object*/
	}
};

console.log(person);
console.log("Result from object methods: ");
person.talk();

/*Add methods to objects*/
person.walk = function(){
	console.log(this.name + " is currently walking.");
}

person['walk']();

let friend = {
	firstname: "Clienton",
	lastname: "Galve",
	address: {
		city: "Jagna",
		country: "Philippines"
	},
	emails: ["clienton@gmail.com", "galve@gmail.com"],
	introduce: function(){
		console.log("Hi! my name is " + this.firstname + " " + this.lastname + ". I lived in " + this.address.city + ", " + this.address.country + ".")
	}
}

friend.introduce();