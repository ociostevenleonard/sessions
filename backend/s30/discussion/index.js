/*
reduce() - more like forloop (increment or sum+=num)
let reduceNumber = numbers.reduce((Sum, NumberElement) => Sum + NumberElement, 0);
console.log(reduceNumber);*/

/*Exponent Operator*/

let firstNum = 8 ** 2;
console.log(firstNum);

let secondNum = Math.pow(8, 2);
console.log(secondNum);

/*Template Literals (``)*/
let name = "John";

let message = `Hello ${name}! Welcome!`;
console.log(message);

/*No need to put \n*/
console.log(`${name} attended a math competition.
	He won it by solving the problem 8**2 with the solution of
	${firstNum}`);


let interestRate = 0.1;
let principal = 1000;

console.log(`The interest on your savings account is: ${principal * interestRate}`);

/*Array Destructuring*/
let fullName = ["Juan", "Dela", "Cruz"];

const [firstname, middlename, lastname] = fullName; 
/*index assigned to variables [Juan -> firstname] [Dela -> middlename] [Cruz -> lastname]*/

console.log(`Hello ${firstname} ${middlename} ${lastname}! It's nice to meet you.`);

/*Object destructuring*/
const person = {
	givenName: "Steven",
	maidenName: "Du",
	familyName: "Ocio"
};

const {givenName, maidenName, familyName} = person;

console.log(`Hello ${givenName} ${maidenName} ${familyName}`);

/*Arrow Function*/
const hello = (name) => {
	console.log(`Hello ${name}`);
};

/*Same as*/
/*function hello(name){
	console.log(`Hello ${name}`);
};*/

hello("Steven");

let students = ["John", "Jane", "Judy"];

students.forEach((studentElement) => {
	console.log(studentElement);
});

/*Implicit Return Statement (hidden return)*/
const add = (x, y) => x + y;

console.log(add(1, 2));


/*Class-Based Object Blueprint (much like a function combined with object)*/
class Car{
	constructor(brand, name, year){
		this.ConstrBrand = brand;
		this.ConstrName = name;
		this.ConstrYear = year;
	}
}

const MyCar = new Car("Ford", "Ranger Raptor", "2021");
console.log(MyCar);







function isPalindrome(str){
	str = str.toLowerCase();
	for(let frontCtr = 0, lastCtr = (str.length-1) ; frontCtr <= lastCtr ; frontCtr++, lastCtr--){
		if(str[frontCtr] !== str[lastCtr]){
			return false;
		}
	}
	return true;
}

console.log(isPalindrome('stets'));
console.log(isPalindrome('steve'));
console.log(isPalindrome(''));