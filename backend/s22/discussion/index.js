function printName(name){
	console.log("My name is " + name);
}

function checkDivisibility(num){
	let remainder = (num % 8);

	console.log("The remainder of "+ num +" divided by 8 is: " + remainder);

	let isDivisibleBy8 = (remainder === 0);

	console.log("Is " + num + " divisible by 8?");
	console.log(isDivisibleBy8);
}

function argumentFunction(){
	console.log("This function was passed as an argument.");
}

function invokeFunction(argument){
	argumentFunction();
}

function createFullname(firstName, middleName, lastName){
	console.log(firstName + " " + middleName + " " + lastName);
}

function printWelcomeMessage(){
	let firstName = prompt("Enter your First Name"), 
	lastName = prompt("Enter your Last Name");

	console.log("Hello, " + firstName + " " + lastName + "!");
	console.log("Welcome to my page!");	
}

function greeting(){
	return "Hello, this is a return statement";
}


printName("Steven");
printName("Leonard");
checkDivisibility(10);
invokeFunction();
createFullname("Steven Leonard", "Du", "Ocio");
printWelcomeMessage();

let greetingFunc = greeting();
console.log(greetingFunc);