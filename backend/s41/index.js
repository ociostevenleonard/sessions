const express = require("express");
const mongoose = require("mongoose");
const port = 3001;
const app = express();
const connectionString = 'mongodb+srv://admin:admin123@cluster0.wzejwel.mongodb.net/B305-to-do?retryWrites=true&w=majority';

mongoose.connect(connectionString, {
	useNewUrlParser : true,
	useUnifiedTopology : true
	// allows us to avoid any current or future errors while connnecting to mongoDB
});

let dbConn = mongoose.connection;
/*If Error*/
dbConn.on("error", console.error.bind(console, "Connection Error!"));
/*If Connected*/
dbConn.on("open", () => console.log("We're connected MongoDB"));


/*Create Schema*/
const taskSchema = new mongoose.Schema({
	// Define fields with corresponding data type
	name: String,
	status: {
		type: String,
		default: "pending"
		// default -> equips predefined values
	}
});
/*Create Table*/
const Task = mongoose.model("Task", taskSchema);


/*Middlewares (Main Code)*/
app.use(express.json());
app.use(express.urlencoded({
	extended:true
}));

// TASK ROUTES

// Business Logic
/*
1. Add a functionality to check if there are duplicate tasks
    - If the task already exists in the database, we return an error
    - If the task doesn't exist in the database, we add it in the database
2. The task data will be coming from the request's body
3. Create a new Task object with a "name" field/property
4. The "status" property does not need to be provided because our schema defaults it to "pending" upon creation of an object
*/



app.post("/tasks", (req, res) => {
	Task.findOne({name: req.body.name}).then((result, error)=>{
		// if document was found or already existing
		if(result != null && result.name === req.body.name){
			return res.send(`Duplicate task found! (_id: ${result._id}`);
		}else{
			let newTask = new Task({
				name: req.body.name
			})

			newTask.save().then((savedTask, saveErr) => {
				if(saveErr){
					return console.error(saveErr);
				}else{
					// if there is no error
					return res.status(201).send("New Task Created!");
				}
			})
		}
	})
})

app.get("/tasks", (request, response) => {
	Task.find({}).then((result, error) => {
		if(error){
			return console.error(error);
		}else{
			return response.status(200).json({
				data: result
			});
			/*return response.send(result);*/
		}
	})
});



/*ACTIVITY*/
const userSchema = new mongoose.Schema({
	username: String,
	password: String
});
const Users = mongoose.model("Users", userSchema);

app.post("/signup", (request, response) => {
	Users.findOne({
		username: request.body.username
	}).then((result, error)=>{
		if(result != null && result.username === request.body.username){
			return response.send(`User ${result.username} already exist.`);
		}else{
			if(request.body.username === "" || request.body.password === ""){
				return response.send("BOTH username and password must be provided.");
			}else{
				let NewUser = new Users({
					username: request.body.username,
					password: request.body.password
				});

				NewUser.save().then((savedUser, saveErr) => {
					if(saveErr){
						return console.error(saveErr);
					}else{
						return response.status(201).send(`New User Created! (username: ${request.body.username})`);
					}
				});
			}
		}
	})
})








/*ACTIVITY END*/


if(require.main === module){
	app.listen(port, () => console.log(`Server is running at port ${port}`));	
}

module.exports = app;