/*JSON - JavaScript Object Notation*/
/*Much like an JS Object*/
/*
	Difference Between Object and JSON
	Object -> "city":
	JSON   -> city:
*/

/*{
	"city": "Quezon City",
	"province": "Metro Manila",
	"country": "Philippines"
}*/

/*JSON Arrays*/
/*"cities": [
		{
			"city" : "Quezon City", 
			"province" : "Metro Manila", 
			"country": "Philippines"
		},
		{
			"city" : "Manila City", 
			"province" : "Metro Manila", 
			"country": "Philippines"	
		},
		{
			"city" : "Makati City", 
			"province" : "Metro Manila", 
			"country": "Philippines"	
		}
];*/

/*JSON Methods*/
let bachesArray = [
	{
		batchName: "Batch X"
	},
	{
		batchName: "Batch Y"
	}
];

/*Stringify method - converts JS Object to JSON*/

console.log("Result from Stringify: ");
console.log(JSON.stringify(bachesArray));

let data = JSON.stringify({
		name: "John",
		age: 31,
		address: {
			city: "Bohol",
			country: "Philippines"
		}
});

console.log(data);

/*Using stringify method with variables*/
let firstName = prompt("Enter First Name: ");
let lastName = prompt("Enter Last Name: ");
let age = prompt("Enter Age: ");
let address = {
		city: prompt("Enter City: "),
		country: prompt("Enter Country: ")
};

let otherData = JSON.stringify({
	JSON_fName: firstName,
	JSON_lName: lastName,
	JSON_Age: age,
	JSON_Address: address
});

console.log(otherData);

/*Converting JSON.stringify to JS Object*/
let batchesJSON = `[{"batchName": "Batch X"}, {"batchName": "Batch Y"}]`;
console.log("Result from parse: ");
console.log(JSON.parse(batchesJSON));