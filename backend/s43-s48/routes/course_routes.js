const express = require("express");
const router = express.Router();
const courseController = require("../controllers/course_controller.js");
const auth = require('../auth.js');
const bcrypt = require('bcrypt');

const {verify, verifyAdmin} = auth;

/*Create Course*/
router.post('/', verify, verifyAdmin, courseController.addCourse);

/*Show All Courses*/
router.get('/all', courseController.ShowAllCourses);

/*Show All Active Courses*/
router.get('/', courseController.ShowAllActiveCourses);

/*Get Specific Course Using ID*/
router.get("/:courseID", courseController.getSpecificCourse);

/*Updating Course (Admin Only)*/
router.put('/:courseID', verify, verifyAdmin, courseController.updateCourse);

/*Archive a course (Admin Only)*/
router.put('/:courseID/archive', verify, verifyAdmin, courseController.archiveCourse);

/*Activate a course (Admin Only)*/
router.put('/:courseID/activate', verify, verifyAdmin, courseController.activateCourse);






module.exports = router;
