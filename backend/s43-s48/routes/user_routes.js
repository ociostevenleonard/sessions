const express = require("express");
const router = express.Router();
const userController = require("../controllers/user_controller.js");
const auth = require('../auth.js');
const bcrypt = require('bcrypt');

const {verify, verifyAdmin} = auth;

/*check email routes*/
router.post('/checkEmail', (request, response) => {
	userController.checkIfEmailExist(request.body.email).then((result_from_controller) => {
		response.send(result_from_controller);
	});
});

/*register user*/
router.post('/register', (request, response) => {
	userController.registerUser(request.body).then((result_from_controller) => {
		response.send(result_from_controller);
	});
});

/*user auth*/
router.post("/login", (request, response) => {
	userController.loginUser(request.body).then((result_from_controller) => {
		if(result_from_controller == null){
			response.send(false);
		}else{
			const isPasswordCorrect = bcrypt.compareSync(request.body.password, result_from_controller.password);
			if(isPasswordCorrect){
				return response.send({
					access: auth.createAccessToken(result_from_controller)
				})
			}else{
				response.send(false);
			}
		}
	})
});




/*Routes*/
/*router.get('/details', verify, (request, response) => {
	userController.getProfile(request.user.id).then((result_from_controller) => {
		if(result_from_controller == null){
			response.send("ID not found!");
		}else{
			result_from_controller.password = "";
			response.send(result_from_controller);
		}
	});
});*/

router.get('/details', verify, userController.getProfile);

router.post('/enroll', verify, userController.checkIfUserIsAlreadyEnrolledToCourse, userController.enroll);
/*router.post('/enroll', verify, userController.enroll);*/

router.get('/getEnrollments', verify, userController.getEntrollments);

/*Reset Password*/
router.put('/reset-password', verify, userController.resetPassword);

module.exports = router;