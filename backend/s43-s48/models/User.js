const mongoose = require("mongoose");

const usersSchema = new mongoose.Schema({
	firstName: {
		type: String,
		required: [true, 'First Name is required!']
	},
	lastName: {
		type: String,
		required: [true, 'Last Name is required!']
	},
	email: {
		type: String,
		required: [true, 'Email is required!']
	},
	password: {
		type: String,
		required: [true, 'Password is required!']
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
	mobileNo: {
		type: String,
		required: [true, 'Mobile No. is required!']
	},
	isDisabled: {
		type: Boolean,
		default: false
	},
	enrollments: [
		{
			courseId: {
				type: String,
				required: [true, 'Course ID is required!']
			},
			name: {
				type: String,
				required: [true, 'Course Name is required!']
			},
			description: {
				type: String,
				required: [true, 'Course description is required!']
			},
			enrolledOn: {
				type: Date,
				default: new Date()
			},
			status: {
				type: String,
				default: "Enrolled"
			}	
		}
	]
});

module.exports = mongoose.model("User", usersSchema);