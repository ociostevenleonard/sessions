const User = require('../models/User.js');
const Course = require('../models/Course.js');
const bcrypt = require('bcrypt');
var mongoose = require('mongoose');

module.exports.checkIfEmailExist = (request_from_user) => {
	return User.find({
		email: request_from_user
	}).then((result) => {
		if(result.length > 0){
			return true;
		}else{
			return false;
		}
	})
};

module.exports.registerUser = (request_from_user) => {
	let newUser = new User({
		firstName: request_from_user.firstName,
		lastName: request_from_user.lastName,
		email: request_from_user.email,
		mobileNo: request_from_user.mobileNo,
		password: bcrypt.hashSync(request_from_user.password, 10)
	});

	return newUser.save().then((savedTask, saveErr) => {
		if(saveErr){
			return false;
		}else{
			return true;
		}
	}).catch(err => err);
}

module.exports.loginUser = (request_from_user) => {
	return User.findOne({
		email: request_from_user.email
	}).then(result => {
		return result;
	});	
};






/*Controller*/
/*module.exports.getProfile = (request_from_user) => {
	return User.findById(request_from_user).then((result, error) => {
		if(error){
			return null;
		}else{
			return result;
		}
	});
};*/

module.exports.getProfile = (request, response) => {
	return User.findById(request.user.id).then((result, error) => {
		if(error){
			response.send(null);
		}else{
			result.password = "";
			response.send(result);
		}
	});
};

module.exports.enroll = async (request, response) => {
	if(request.user.isAdmin){
		return response.send("Admin isn't allowed to enroll.");
	}

	let isUserUpdated = await User.findById(request.user.id).then((user_result, error) => {
		/*To check if already enrolled*/
		/*
		-Loop-
		let UserEnrollments = user_result.enrollments;		
		let isAlreadyEnrolled = false;
		for(let index = 0 ; index < UserEnrollments.length ; index++){
			if(UserEnrollments[index].courseId == request.body.courseID){
				isAlreadyEnrolled = true;
				break;
			}
		}
		if(isAlreadyEnrolled){
				return response.send(`Course ID ${request.body.courseID} is already enrolled to ${request.user.id}`);
		}*/
		/*
		-some-
		let isAlreadyEnrolled = user_result.enrollments.some(items => {
			return (items.courseId == request.body.courseID);
		});
		if(isAlreadyEnrolled){
			return response.send(`Course ID ${request.body.courseID} is already enrolled to ${request.user.id}`);
		}*/
		/*end*/

		return Course.findById(request.body.courseID).then((result) => {
			let newEnrollment = {
				courseId: request.body.courseID,
				name: result.name,
				description: result.description
			};
			
			user_result.enrollments.push(newEnrollment);

			return user_result.save().then((result, error) => {
				if(error){
					return response.send(error);
				}else{
					return true;
				}
			}).catch(err => response.send(err));
		});
	});

	if(!isUserUpdated){
		return response.send({
			message: isUserUpdated
		});
	}

	let isCourseUpdated = await Course.findById(request.body.courseID).then((course_result, error) => {
		let enrollee = {
			userId: request.user.id
		}

		course_result.enrollees.push(enrollee);

		return course_result.save().then((result, error) => {
			if(error){
				return response.send(error);
			}else{
				return true;
			}
		}).catch(err => response.send(err));
	});

	if(!isCourseUpdated){
		return response.send({
			message: isCourseUpdated
		});
	}

	if(isUserUpdated && isCourseUpdated){
		response.send(`You are now enrolled to courseID ${request.body.courseID}`);
	}
};


module.exports.checkIfUserIsAlreadyEnrolledToCourse = (request, response, next) => {
	return User.findById(request.user.id).then((result, error) => {
		let isAlreadyEnrolled = false;
		for(let index = 0 ; index < result.enrollments.length; index++){
			if(result.enrollments[index].courseId === request.body.courseID){
				isAlreadyEnrolled = true;
				break;
			}
		}
		if(isAlreadyEnrolled){
			return response.send(`Course ID ${request.body.courseID} is already enrolled to ${request.user.id}`);
		}else{
			next();
		}
	});
};

module.exports.getEntrollments = (request, response) => {
	return User.findById(request.user.id).then((result) => {
		if(result.enrollments.length < 1){
			return response.send("No Courses Enrolled.");
		}else{
			return response.send(result.enrollments);
		}
	});
};

module.exports.resetPassword = async (request, response) => {
	try{
			const newPassword = request.body.newPassword;
			const userId = request.user.id;

			const hashedPassword = await bcrypt.hash(newPassword, 10);

			await User.findByIdAndUpdate(userId, {
				password: hashedPassword
			});

		response.status(200).json({ message: "Password reset successfully." });
	}catch(error){
		response.status(500).json({ message: "Internal Server Error" });
	}
};
