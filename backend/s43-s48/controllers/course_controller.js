const Course = require('../models/Course.js');
const bcrypt = require('bcrypt');
var mongoose = require('mongoose');

/*Create Course*/
module.exports.addCourse = (request, response) => {
	let newCourse = new Course({
		name: request.body.course_name,
		description: request.body.course_desc,
		price: request.body.course_price
	});

	return newCourse.save().then((savedCourse, saveErr) => {
		if(saveErr){
			return response.send(false);
		}else{
			return response.send(true);
		}
	}).catch(err => err);
};

/*View All Courses*/
module.exports.ShowAllCourses = (request, response) => {
	return Course.find({}).then(result => {
		if(result.length < 1){
			return response.send('No Courses Recorded');
		}else{
			return response.send(result);
		}
	});
};

/*View All Active Courses*/
module.exports.ShowAllActiveCourses = (request, response) => {
	return Course.find({
		isActive: true
	}).then(result => {
		if(result.length < 1){
			return response.send('No Courses Recorded');
		}else{
			return response.send(result);
		}
	});
};

/*Get Specific Course Using ID*/
module.exports.getSpecificCourse = (request, response) => {
	return Course.findById(request.params.courseID).then(result => {
		if(result.length < 1){
			return response.send('Course not found.');
		}else{
			if(!result.isActive){
				return response.send(`Course ID ${result._id} is not available`);
			}else{
				return response.send(result);
			}
		}
	}).catch(error => response.send("Please enter a correct a course ID"));
};

/*Update Course Using ID*/
module.exports.updateCourse = (request, response) => {
	let updatedCourse = {
		name: request.body.name,
		description: request.body.desc,
		price: request.body.price
	};
	return Course.findByIdAndUpdate(request.params.courseID, updatedCourse).then((course, error) => {
		if(error){
			return response.send(false);
		}else{
			return response.send(true);
		}
	}).catch(error => response.send("Please enter a correct a course ID"));
};

/*Archive Course Using ID*/
module.exports.archiveCourse = (request, response) => {
	let updatedCourse = {
		isActive: false
	};
	return Course.findByIdAndUpdate(request.params.courseID, updatedCourse).then((course, error) => {
		if(error){
			return response.send(false);
		}else{
			return response.send(true);
		}
	}).catch(error => response.send("Please enter a correct a course ID"));
};

/*Activate Course Using ID*/
module.exports.activateCourse = (request, response) => {
	let updatedCourse = {
		isActive: true
	};
	return Course.findByIdAndUpdate(request.params.courseID, updatedCourse).then((course, error) => {
		if(error){
			return response.send(false);
		}else{
			return response.send(true);
		}
	}).catch(error => response.send("Please enter a correct a course ID"));
};