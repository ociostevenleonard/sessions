/*Dependencies and Modules*/
const jwt = require('jsonwebtoken');
const secret = "CourseBookingAPI";

/*Token Creation*/
module.exports.createAccessToken = (user) => {
	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	}
	return jwt.sign(data, secret, {});
}




/*to check if user has token ID*/
module.exports.verify = (request, response, next) => {
	/*
		POSTMAN
			Auth
				-> Bearer Token
	*/

	console.log("1->" + request.headers.authorization);

	let token = request.headers.authorization;

	if(typeof token === "undefined"){
		return response.send({
			auth: "Failed, No Token"
		});
	}else{
		console.log("2->" + token);
		//token = token.slice(7, token.length);
		
			token = token.substring(7, token.length);
		
		console.log("3->" + token);

		jwt.verify(token, secret, (error, decodedToken) => {
			if(error){
				return response.send({
					auth: "Failed",
					message: error.message
				});
			}else{
				console.log(decodedToken);
				request.user = decodedToken;

				next();
				//Will let us proceed to the next controller
			}
		});
	}
};


module.exports.verifyAdmin = (req, res, next) => {
  if (req.user.isAdmin) {
    next();
  } else {
    return res.send({
      auth: `Failed`,
      message: `Action Forbidden`,
    });
  }
};
/*Another Way*/
/*module.exports.verifyAdmin = (request, response, next) => {
	let token = request.headers.authorization;
	token = token.substring(7, token.length);
	jwt.verify(token, secret, (error, decodedToken) => {
		if(error){
			return response.send({
				auth: "Failed",
				message: error.message
			});
		}else{
			request.user = decodedToken;
			if(request.user.isAdmin){
				next();
			}else{
				response.send({
					auth: "Failed",
					message: "Action Forbidden"
				})
			}
		}
	});
};*/