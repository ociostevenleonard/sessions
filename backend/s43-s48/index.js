/*Depencies*/
const dotenv = require('dotenv').config();
const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
const app = express();


/*Server Setup*/
app.use(express.json());
app.use(express.urlencoded({
	extended:true
}));
app.use(cors());
/*Routes*/
app.use("/users", require('./routes/user_routes.js'));
app.use("/courses", require('./routes/course_routes.js'));


/*MongoDB Connection*/
mongoose.connect(process.env.MongoDB_ConnectionString, {
	useNewUrlParser : true,
	useUnifiedTopology : true
});
mongoose.connection.once("open", () => console.log("We're connected to MongoDB"));




/*Server Gateway Response*/
if(require.main === module){
	app.listen(process.env.PORT, () => {
		console.log(`API is now running on port ${process.env.PORT}`);
	});
}

module.exports = app;