/*MongoDB CRUD Operations*/
/*
	Create - (MySQL -> INSERT)
	db.users.insertOne({})

	B305.users.insertOne({
		firstName: "Steven Leonard",
		lastName: "Ocio",
		age: 26,
		contact: {
			phone: "09196385111",
			email: "ociostevenleonard@gmail.com"
		},
		courses: ["CSS", "JS", "PYTHON"],
		department: "None"
	});

	insertMany([{object}, {object}]) (Array Parameter)
*/
db.users.insertMany([
	{
		firstName: "Stephen",
		lastName: "Hawking",
		age: 76,
		contact: {
			phone: "09196385111",
			email: "stephenhawking@gmail.com"
		},
		courses: ["PHP", "React", "PYTHON"],
		department: "None"
	},
	{
		firstName: "Neil",
		lastName: "Armstrong",
		age: 82,
		contact: {
			phone: "09196385111",
			email: "armstrongNeil@gmail.com"
		},
		courses: ["React", "Laravel", "Sass"],
		department: "None"
	}
]);

/*
	Read - (MySQL -> SELECT)
	MySQL - LIKE %tev% => MongoDB - /.(askterisk symbol *)tev.(askterisk symbol *)/ 

	-> db.users.find({field: value})
	
*/
db.users.find({
	department: "None",
	age: 32
});


/*
	Update - (MySQL -> SELECT)
	db.users.updateOne({criteria}, {$set: {field: value}})
*/
db.users.updateOne(
	{firstName: "Bill"}, 
	{
		$set: {
			firstName: "Bill",
			lastName: "Gates",
			age: 65,
			contact: {
				phone: "1234567890",
				email: "billgates@gmaail.com"
			},
			courses: ["PHP", "Laravel"],
			department: "Operations",
			status: "Active"
		} 
	}
	);
db.users.updateMany(
	{
		department: "HR"
	}, 
	{
		$set: {
			status: "Inactive"
		} 
	}
);	

/*Delete - */

db.users.deleteOne(
{
	firstName: "Steven",
	lastName: "Ocio"
}
);

