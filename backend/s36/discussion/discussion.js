/*
	$ave -> average
	$sum -> sum
	$max -> max amount
	$min -> min amount
*/


db.friuts.insertMany([
		{
		        name : "Apple",
		        color : "Red",
		        stock : 20,
		        price: 40,
		        supplier_id : 1,
		        onSale : true,
		        origin: [ "Philippines", "US" ]
		      },

		      {
		        name : "Banana",
		        color : "Yellow",
		        stock : 15,
		        price: 20,
		        supplier_id : 2,
		        onSale : true,
		        origin: [ "Philippines", "Ecuador" ]
		      },

		      {
		        name : "Kiwi",
		        color : "Green",
		        stock : 25,
		        price: 50,
		        supplier_id : 1,
		        onSale : true,
		        origin: [ "US", "China" ]
		      },

		      {
		        name : "Mango",
		        color : "Yellow",
		        stock : 10,
		        price: 120,
		        supplier_id : 2,
		        onSale : false,
		        origin: [ "Philippines", "India" ]
		      }
	]);



/*

Aggregate Methods

SYNTAX
 {$match: {field: value}}

	db.collectionName.aggregate([
		{
			$match: {
				fieldA: valueA
			}
		},
		{
			$group: {
				_id: "$fieldB",
				result : {
					operation
				}
			}
		}
	]);


MySQL -> GROUP BY and SUM()
*/
db.friuts.aggregate([
	{
		$match: {
			onSale: true
		}
	},
	{
		$group: {
			_id: "$supplier_id",
			total: {
				$sum: "$stock"
			}
		}
	}
]);

/*
	Field Projection with Aggregation

	$project - include/exclude display
	0 -> exclude (no show)
	1 -> include (show)
*/

db.friuts.aggregate([
	{
		$match: {
			onSale: true
		}
	},
	{
		$group: {
			_id: "$supplier_id",
			total: {
				$sum: "$stock"
			}
		}
	},
	{
		$project: {
			_id: 0
		}
	}
]);

/*
	ORDER BY
	-1 -> DESC
	1  -> ASC
*/
db.friuts.aggregate([
	{
		$match: {
			onSale: true
		}
	},
	{
		$group: {
			_id: "$supplier_id",
			total: {
				$sum: "$stock"
			}
		}
	},
	{
		$project: {
			_id: 0
		}
	},
	{
		$sort: {
			total: -1
		}
	}
]);

/*
	Aggregating results based on array fields

	{
		$unwind: field
	}
*/
db.friuts.aggregate([
	{
		$unwind: "$origin"	
	}
]);



/*GROUP BY origin*/
db.friuts.aggregate([
	{
		$unwind: "$origin"	
	},
	{
		$group: {
			_id: "$origin",
			kinds: {
				$sum: 1
			}
		}
	}
]);