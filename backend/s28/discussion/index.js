let countries = ["US","PH","CAN","SG", "TH", "PH", "FR", "DE"];

/*Get index number*/
console.log("Result of indexOf " + countries.indexOf("PH"));

/*Gets the last index*/
console.log("Result of LastIndexOf " + countries.lastIndexOf("PH"));

/*Gets the last index and counts backwards from the last index*/
console.log("Result of LastIndexOf " + countries.lastIndexOf("PH", 3));

/*Gets the index starting from the parameter up to the last index*/
console.log("Result of slice " + countries.slice(2));

/*Gets the index starting from the parameter up to the second parameter*/
console.log("Result of slice (2 params)\n " + countries.slice(2, 4));

/*Gets the index starting from the last parameter*/
console.log("Result of slice (negative)" + countries.slice(-3));

/*Arrays can be converted to string also using toString() method*/



/*Concats array element*/
let taskArrayA = ["drink html", "eat javascript"];
let taskArrayB = ["inhale css", "breathe sass"];
let task = taskArrayA.concat(taskArrayB);
console.log(task);

/*join() - converts to string with a spacer*/
let users = ["John", "Jane", "Joe", "Robert"];
console.log(users.join(""));


let filteredTask = [];
/*forEach() - much easier but maka bolok kay spoon fed ra kaayo, prefer to use normal for loop for more challenge*/
task.forEach((ArrayElement) => {
	if(ArrayElement.length > 8){
		filteredTask.push(ArrayElement);
	}
});

console.log("Result of filtered task");
console.log(filteredTask);


/*every() - like a for loop with an if statement inside.*/
let numbers = [1, 2, 3, 4, 5];
let allValid = numbers.every((number) => {
	return (number < 3);
});
console.log(allValid);

/*some() - more like every() but does not automatically returns*/
let someValid = numbers.some((number) => {
	return (number < 3);
});
console.log(someValid);

/*filter() - much like SQL queries [loop]*/
let filterValid = numbers.filter((itemOnIndex) => {
	return (itemOnIndex < 3);
});
console.log(filterValid);

/*include() - check if existing*/
let products = ["Mouse", "Keyboard", "Laptop", "Monitor"];

let productFound1 = products.includes("Mouse");
console.log(productFound1);

/*example include() + filter()*/
let filteredProducts = products.filter((productName) => {
	return productName.toLowerCase().includes("m");
});

console.log(filteredProducts);


/*console.log(mySlice(-2, countries));
console.log(myEvery(numbers));
console.log(mySome(numbers));
*/


function myEvery(myArray){
	for(let index = 0 ; index < myArray.length; index++){
		if(!(myArray[index] < 3)){
			return false;
		}
	}
	return true;
}

function mySome(myArray){
	for(let index = 0 ; index < myArray.length; index++){
		if((myArray[index] < 3)){
			return true;
		}
	}
	return false;
}

function mySlice(myNumber, myArray){
	let myReturnArray = [];
	if (myNumber < 0){
		myNumber += (myArray.length - 1) + 1;
	}
	for( ; myNumber < myArray.length; myNumber++){
		myReturnArray.push(myArray[myNumber]);
	}	
	return myReturnArray;
}