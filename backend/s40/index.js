const express = require('express');
const app = express();
const port = 4000;

/*Middlewares (Server setup)*/
/*Shortcut -> express().use(require('express').json())*/

/*Allows your app to read data from forms*/
app.use(express.json());

/*It accepts different datatypes from user*/
app.use(express.urlencoded({
	extended: true
}));

/*Routes Example*/

/*--GET Method--*/
app.get("/", (request, response) => {
	response.send("Hello World!");
});
app.get("/hello", (request, response) => {
	response.send("Hello Again!");
});

/*Check users array*/

app.get("/check-array", (request, response) => {
	response.send(users);
});

/*--POST Method--*/
app.post("/hello", (request, response) => {
	response.send(`Hello there, ${request.body.firstName} ${request.body.lastName}!`);
});

let users = [];
app.post("/signup", (request, response) => {	
	console.log(request.body);
	if(request.body.username !== "" && request.body.password !== ""){
		let doesExist = false;
		for(let index = 0 ; index < users.length ; index++){
			if(request.body.username === users[index].username){
				doesExist = true;
				break;
			}
		}
		if(doesExist){
			response.send("User already exist");
		}else{
			users.push(request.body)	
			response.send(`Hi ${request.body.username}`);
		}


	}else{
		response.send("Username or Password is missing.");
	}
});

/*Change Password*/
app.put("/change-password", (request, response) => {
	let message = "No user is registered.";
	if(users.length > 0){
		message = "User does not exist.";
		for(let index = 0; index < users.length; index++){
			if(users[index].username === request.body.username){
				users[index].password = request.body.password;
				message = `User ${request.body.username}'s password has been updated.`
				break;
			}
		}
	}
	response.send(message);			
});

if(require.main === module){
	app.listen(port, () => console.log(`Server is running at port ${port}`));
}