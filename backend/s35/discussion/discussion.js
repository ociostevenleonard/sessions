/*Comparison Query Operator

	$gt  -> > (Greater Than)
	$gte -> >= (Greater Than or Equal)
	-SYNTAX-
	db.collectionname.find({field: {$gt : value}})


	$lt  -> < (Less Than)
	$lte -> <= (Less Than or Equal)
	-SYNTAX-
	db.collectionname.find({field: {$lt : value}})


	$ne  -> != (Not Equal)
	-SYNTAX-
	db.collectionname.find({field: {$ne : value}})

	$in  -> MySQL IN()
	-SYNTAX-
	db.collectionname.find({field: {$in : [values]}})
*/ 
db.users.find({
	age: {
		$gt: 50
	}
});

db.users.find({
	age: {
		$lt: 50
	}
});

db.users.find({
	age: {
		$ne: 76
	}
});

db.users.find({
	lastName: {
		$in: ["Hawking", "Doe"]
	}
});


/*
	Logical Query Operator
	MySQL OR
	-SYNTAX-
	db.collectionname.find({
		$or : [
			{
				fieldA: value
			},
			{
				fieldB: value
			}
		]
	})


	MySQL AND
	-SYNTAX-
	db.collectionname.find({
		$and : [
			{
				fieldA: value
			},
			{
				fieldB: value
			}
		]
	})
*/

db.users.find({
		$or : [
			{
				firstName: 'Neil'
			},
			{
				age: 25
			}
		]
	});

/*WHERE (firstName = 'Neil') OR (age > 30)*/
db.users.find({
		$or : [
			{
				firstName: 'Neil'
			},
			{
				age: {
					$gt: 30
				}
			}
		]
	});


/*WHERE (age != 82) AND (age != 76)*/
db.users.find({
		$and : [
			{
				age: {
					$ne : 82
				}
			},
			{
				age: {
					$ne: 76
				}
			}
		]
	});

/*WHERE firstName = 'Neil' AND (age = 82 OR age = 76)*/
db.users.find({
		$and : [
			{
				firstName: "Neil"
			},
			{
				$or: [
					{
						age: 11
					},
					{
						age: 76
					}
				]
			}
		]
	});

/*
	SELECT firstName, lastName, contact FROM users WHERE firstName = "Jane"

	field: 1 -> Show Column
	field: 0 -> Hide Column
*/
db.users.find(
	{
		firstName: "Jane"
	},
	{
		firstName: 1,
		lastName: 1,
		contact: 1
	}
);

db.users.find(
	{
		firstName: "Jane"
	},
	{
		firstName: 1,
		lastName: 1,
		contact: 1,
		_id: 0
	}
);

/*
	Evaluation Query Operator
	MySQL LIKE %string%
	-SYNTAX-
	db.users.find(
		{
			field: {
				$regex: "pattern",
				$options: $optionValue
			}
		}
	)

*/

/*CASE SENSITIVE*/
db.users.find({
	firstName : {
		$regex: "N"
	}
});


/*
	CASE INSENSITIVE
	$options: "i" -> insensitive
*/
db.users.find({
	firstName : {
		$regex: "n",
		$options: "i"
	}
});