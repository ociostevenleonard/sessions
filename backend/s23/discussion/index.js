let numA = -1;

if (numA < 0) {
	console.log("Hello");
}else{
	console.log("Hi");
}


let city = "New York";

if(city == "New York"){
	console.log("Welcome to New York City!");
}

let numH = 1;
if(numH < 0){
	console.log("Hello!");
}else if(numH > 0){
	console.log("World!");
}


city = "Tokyo";

if(city == "New York"){
	console.log("Welcome to New York City!");
}else if(city == "Tokyo"){
	console.log("Welcome to Tokyo Japan!");
}


let num1 = 5;

if(num1 === 0){
	console.log("Hello!");
}else if(num1 === 3){
	console.log("World!");
}else{
	console.log("Sorry, all conditions are not met.");
}



function determineTyphoonIntensity(windSpeed){
	if(windSpeed < 30){
		return "Not a Typhoon Yet!";
	}else if(windSpeed <= 61){
		return "Tropical Depression Detected!";
	}else if(windSpeed <= 88){
		return "Tropical Storm Detected!";
	}else if(windSpeed <= 117){
		return "Severe Tropical Storm Detected!";
	}else{
		return "Typhoon Detected!";
	}
}

determineTyphoonIntensity(10)


if(true){
	console.log("truthy");
}

let ternary = (1 < 18) ? "Yes" : "No";
console.log(ternary);

let name;

function isOfLegalAge(){
	name = "John";
	return "You are of the legal age limit.";
}
function isUnderAge(){
	name = "Jane";
	return "You are under the age limit.";
}

/*Parsing*/
let age = parseInt(prompt("What is your age?"));
console.log(age);
let legalAge = (age >= 18) ? isOfLegalAge() : isUnderAge();
console.log("Result of Ternary Operator in functions: " + legalAge + ", " + name);

let day = prompt("What day of the week is it today?").toLowerCase();

switch(day){
	case "monday":
		console.log("The color of the day is red.");
		break;
	case "tuesday":
		console.log("The color of the day is orange.");
		break;
	case "wednesday":
		console.log("The color of the day is yellow.");
		break;
	case "thursday":
		console.log("The color of the day is green.");
		break;
	case "friday":
		console.log("The color of the day is blue.");
		break;
	case "saturday":
		console.log("The color of the day is indigo.");
		break;
	case "sunday":
		console.log("The color of the day is violet.");
		break;
	default:
		console.log("Please enter a valid day.");
		break;
}