//[ SECTION ] Functions
    // Functions in javascript are lines/blocks of codes that tell our device/application to perform a certain task when called/invoked
    // Functions are mostly created to create complicated tasks to run several lines of code in succession
    // They are also used to prevent repeating lines/blocks of codes that perform the same task/function


//Function declarations
    //(function statement) defines a function with the specified parameters.

    /*
    Syntax:
        function functionName() {
            code block (statement)
        }
    */
    // function keyword - used to defined a javascript functions
    // functionName - the function name. Functions are named to be able to use later in the code.
    // function block ({}) - the statements which comprise the body of the function. This is where the code to be executed.


    function printName(){
    	console.log("My name is Steven");
    }

    //[ SECTION ] Function Invocation
    //The code block and statements inside a function is not immediately executed when the function is defined. The code block and statements inside a function is executed when the function is invoked or called.
    //It is common to use the term "call a function" instead of "invoke a function".
    printName();

    //[ SECTION ] Function declarations vs expressions
    //Function Declarations

        //A function can be created through function declaration by using the function keyword and adding a function name.

        //Declared functions are not executed immediately. They are "saved for later use", and will be executed later, when they are invoked (called upon).
        
        //declaredFunction(); //declared functions can be hoisted. As long as the function has been defined.

        //Note: Hoisting is Javascript's behavior for certain variables and functions to run or use them before their declaration.

        //declaredFunction();

        function declaredFunction(){
        	console.log("Hello World from declaredFunction()");
        }

		declaredFunction();

		//Function Expression
		        //A function can also be stored in a variable. This is called a function expression.

		        //A function expression is an anonymous function assigned to the variableFunction

		        //Anonymous function - a function without a name.

		        //variableFunction();
		        /* 
		            error - function expressions, being stored 
		            in a let or const variable, cannot be hoisted.
		        */


let variableFunction = function(){
	console.log("Hello again!");
};

variableFunction();


//[ SECTION ] Function scoping

/*  
    Scope is the accessibility (visibility) of variables.
    
    Javascript Variables has 3 types of scope:
        1. local/block scope
        2. global scope
        3. function scope
            JavaScript has function scope: Each function creates a new scope.
            Variables defined inside a function are not accessible (visible) from outside the function.
            Variables declared with var, let and const are quite similar when declared inside a function
*/  

{
	let localVar = "Shinichi Kudo";
}

let globalVar = "Son Goku";

function showNames(){
	var functionVar = "Tony";
	const functionConst = "Steve";
	let functionLet = "Thor";

	console.log(functionVar);
	console.log(functionConst);
	console.log(functionLet);

	console.log(globalVar);
}

showNames();

//Nested Functions

        //You can create another function inside a function. This is called a nested function. This nested function, being inside the myNewFunction will have access to the variable, name, as they are within the same scope/code block.

        function myNewFunction(){
        	let name = "Owen";

        	function nestedFunction(){
        		let nestedName = "Moon";
        		console.log(name);
        	}


        	nestedFunction();
        }

        myNewFunction();

        function myNewFunction2(){
        	myNewFunction();

        	console.log("This is new function 2");
        }

		myNewFunction2();

		//alert("Hello B305!");

		function showSampleAlert(){
			alert("Hello User");
		}

		//showSampleAlert();

		console.log("I will only log in the console when the alert is dismissed");

		// let samplePrompt = prompt("Enter your name: "); /*Must store to a variable*/
		// console.log("Hello " + samplePrompt);

		/*let sampleNullPrompt = prompt("Do Not Enter Anything!");

		console.log(sampleNullPrompt);*/

		//prompt() returns an empty string when there is no input. Or null if the user cancels the prompt().


		//RETURN STATEMENT
		function returnFunc(){
			let name = "I am a return statement";
			return name;
		}

		function returnName(){
			return "Owen Orange";
		}

		let name = returnName();
		console.log(name);

		// [SECTION] RETURN STATEMENT
	// "return" statement allows us to output a value from a function to be passed to the line/block of code that invoked/called the function
  
  
    //Avoid pointless and inappropriate function names.

        function foo(){

            console.log(25%5);

        };

        foo();

    //Name your functions in small caps. Follow camelCase when naming variables and functions.

        function displayCarInfo(){

            console.log("Brand: Toyota");
            console.log("Type: Sedan");
            console.log("Price: 1,500,000");

        }
        
        displayCarInfo();

	// "return" statement also stops the execution of the function and any code after the return statement will not be executed.