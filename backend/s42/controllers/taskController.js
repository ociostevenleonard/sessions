const TaskModel = require("../models/Task.js");

module.exports.getAllTask = () => {
	return TaskModel.find({}).then(result => {
		return result;
	})
};

module.exports.createTask = (requestBody) => {
	let newTask = new TaskModel({
		name: requestBody.name
	});

	return newTask.save().then((savedTask, saveErr) => {
		if(saveErr){
			console.error(saveErr);
			return savedTask;
		}else{
			return savedTask;
		}
	});
};

module.exports.deleteTask = (requestParams) => {
	return TaskModel.findByIdAndRemove(requestParams).then((removedTask, error) => {
		if(error){
			console.error(error);
		}else{
			return removedTask;
		}
	});
};

module.exports.updateTask = (requestParams, requestBody) => {
	return TaskModel.findById(requestParams).then((result, error) => {
		if(error){
			console.error(error);
		}else{
			result.name = requestBody.name;
			return result.save().then((savedTask, saveErr) => {
				if(saveErr){
					console.error(saveErr);
				}else{
					return savedTask;
				}
			});
		}
	});
};


/*Activity*/
module.exports.getSpecificTask = (requestParams) => {
	return TaskModel.findById(requestParams).then((result, error) => {
		if(error){
			console.error(error);
		}else{
			return result;
		}
	});
};

module.exports.updateTaskStatus = (requestParams) => {
	return TaskModel.findById(requestParams.id).then((result, error) => {
		if(error){
			console.error(error);
		}else{
			result.status = requestParams.stt;
			return result.save().then((savedTask, saveErr) => {
				if(saveErr){
					console.error(saveErr);
				}else{
					return savedTask;
				}
			});
		}
	});

	/*
	[ANOTHER WAY]
	return TaskModel.findByIdAndUpdate(requestParams.id, {
		status: requestParams.stt
	}).then((updateTask, error) => {
		if(error){
			console.error(error);
		}else{
			return updateTask;
		}
	});

	*/
};