const express = require("express");
const mongoose = require("mongoose");
const taskRoute = require("./routes/taskRoute.js");
const app = express();
const port = 4000;

app.use(express.json());
app.use(express.urlencoded({
	extended:true
}));
app.use("/tasks", taskRoute);


const connectionString = 'mongodb+srv://admin:admin123@cluster0.wzejwel.mongodb.net/B305-to-do?retryWrites=true&w=majority';
mongoose.connect(connectionString, {
	useNewUrlParser : true,
	useUnifiedTopology : true
});

let dbConn = mongoose.connection;
dbConn.on("error", console.error.bind(console, "Connection Error!"));
dbConn.on("open", () => console.log("We're connected MongoDB"));

if(require.main === module){
	app.listen(port, () => console.log(`Server is running at port ${port}.`));
}
