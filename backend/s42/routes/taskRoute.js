const express = require("express");

/*Allows access to HTTP Methods and Middlewares*/
const router = express.Router();
const taskController = require("../controllers/taskController.js");

/*Get all task*/
router.get("/", (request, response) => {
	taskController.getAllTask().then((resultFromController) => {
		response.send(resultFromController);
	});
});

/*Create Task*/
router.post("/", (request, response) => {
	taskController.createTask(request.body).then((resultFromController) => {
		response.send(resultFromController);
	});
});

/*Delete Task*/
/*: -> Wildcard*/
router.delete("/:id", (request, response) => {
	taskController.deleteTask(request.params.id).then((resultFromController) => {
		response.send(resultFromController);
	});
});

/*Update Task*/
router.put("/:id", (request, response) => {
	taskController.updateTask(request.params.id, request.body).then((resultFromController) => {
		response.send(resultFromController);
	});
});

/*Get Specific*/
router.get("/:id", (request, response) => {
	taskController.getSpecificTask(request.params.id).then((resultFromController) => {
		response.send(resultFromController);
	});
});
/*Update Task Status*/
router.put("/:id/:stt", (request, response) => {
	taskController.updateTaskStatus(request.params).then((resultFromController) => {
		response.send(resultFromController);
	});
});
module.exports = router;