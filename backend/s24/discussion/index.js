//let count = 5;

/*while(count !== 0){
	console.log("While: " + count);
	count--;
}*/

/*Number() equivalent to parseInt()*/

/*let number = Number(prompt("Give me a number."));
do{
	console.log("Do While: " + number);
	number++;
}while(number <= 10);

for(let count = 0 ; count <= 20; count++){
	console.log(count);
}
*/
let myString = "Alex";
/*console.log(myString.length);
console.log(myString[0]);
console.log(myString[1]);
console.log(myString[2]);
console.log(myString[3]);*/
for(let index = 0; index < myString.length ; index++){
	console.log(myString[index]);
}

let myName = "AlEx";
let myNewName = "";
for(let index = 0 ; index < myName.length; index++){
	/*console.log(myName[index]);*/
	if(	myName[index].toLowerCase() == "a" || 
		myName[index].toLowerCase() == "e" || 
		myName[index].toLowerCase() == "i" || 
		myName[index].toLowerCase() == "o" || 
		myName[index].toLowerCase() == "u"){
		console.log(3);
	}else{
		console.log(myName[index]);
	}
}

for(let count = 0 ; count <= 20 ; count++){
	if(count % 2 === 0){
		continue; /*makes another loop*/
	}
	console.log("continue and break: " + count);
	if(count > 10){
		break;
	}
}

let name = "alexandro";
for(let index = 0 ; index < name.length ; index++){
	if(name[index].toLowerCase() === "a"){
		console.log("Continue to the next iteration");
		continue;
	}
	console.log(name[index]);
	if(name[index] == "d"){
		break;
	}
}