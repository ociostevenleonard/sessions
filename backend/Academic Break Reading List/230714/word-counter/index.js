document.getElementById("text").addEventListener("input", GetWords);
document.getElementById("btn").addEventListener("click", convertPasswordType);



function GetWords(){
	let Words = document.getElementById("text").value;
	let wordCounter = CountWords(Words);
	document.getElementById("stat").innerHTML = `You've written ${wordCounter.WordCount} word(s) and ${wordCounter.Str_length} character(s)`;
}

function convertPasswordType(){
	let ptype = document.getElementById("pword");
	let new_ptype = (ptype.type === "password") ? "text" : "password";
	ptype.type = new_ptype;
	
	let btn_label = (ptype.type === "password") ? "Show Password" : "Hide Password";
	document.getElementById("btn").innerHTML = btn_label;
}

function CountWords(StringWords){
	StringWords = StringWords.trim();
	let WCount = (StringWords.length > 0) ? 1 : 0;
	let StringHolder = "";
	for(let index = 0 ;index < StringWords.length; index++){
		if(StringWords[index] != " "){
			StringHolder += StringWords[index];
		}else{
			if(StringHolder != ""){
				WCount++;
				StringHolder = "";
			}
		}
	}
	return {
		Str_length: StringWords.length,
		WordCount: WCount
	};
}