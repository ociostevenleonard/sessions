let color = ["Blue","Red","Yellow","Orange","Green","Purple"];
let planet = ["Venus","Mars","Earth"];
let myFavoriteSingers = ["Earth, Wind, and Fire", "Bee Gees", "Christopher Cross"];
let colorString = (colorArray) => {
	return colorArray.join(", ");
};

let newPlanet = (StringPlanet) => {
	planet.unshift(StringPlanet);
	return planet;
};

let removeLastIndexAndAdd2 = (NewSinger1, NewSinger2) => {
	myFavoriteSingers.splice(myFavoriteSingers.length - 1, 1, NewSinger1, NewSinger2);
	return myFavoriteSingers;
};

/*#1*/
console.log(colorString(color));

/*#2*/
console.log(newPlanet("Mercury"));

/*#3*/
console.log("Array elements of myFavoriteSingers:");
console.log(myFavoriteSingers);
console.log("Array elements of myFavoriteSingers after splice:");
console.log(removeLastIndexAndAdd2("Toto", "Bruno Mars"));
