let gradeConversion = (grade) => {
	let gradeToNumber = Number(grade);
	if(gradeToNumber <= 80 ){
		return `Your grade is ${grade} which means you are a Novice`;
	}else if(gradeToNumber <= 90){
		return `Your grade is ${grade} which means you are an Apprentice`;
	}else if(gradeToNumber <= 100){
		return `Your grade is ${grade} which means you are a Master`;
	}else{
		return "Input Error";
	}
};

console.log(gradeConversion(prompt("Enter Grade")));