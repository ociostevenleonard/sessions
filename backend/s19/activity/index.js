let firstName = "Steven Leonard";
let lastName = "Ocio";
let age = 26;
let hobbies = ["Coding", "Basketball", "Swimming"];
let workAddress = {
	houseNumber: 11,
    street: "National Road",
    city: "Jagna",
    state: "Bohol"
};


console.log("First Name: " + firstName);
console.log("Last Name: " + lastName);
console.log("Age: " + age);
console.log("Hobbies:");
console.log(hobbies);
console.log("Work Address:");
console.log(workAddress);


    let fullName = "Steve Rogers";
    console.log("My full name is: " + fullName);

    let currentAge = 40;
    console.log("My current age is: " + currentAge);
    
    let friends = ["Tony", "Bruce", "Thor", "Natasha", "Clint", "Nick"];
    console.log("My Friends are: ");
    console.log(friends);

    let profile = {
        username: "captain_america",
        fullName: "Steve Rogers",
        age: 40,
        isActive: false,

    };
    console.log("My Full Profile: ")
    console.log(profile);

    let fullName2 = "Bucky Barnes";
    console.log("My bestfriend is: " + fullName2);

    const lastLocation = "Arctic Ocean";
    console.log("I was found frozen in: " + lastLocation);