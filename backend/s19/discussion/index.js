console.log("Hello World!");

/*[SECTION] Syntax, Statements and Comments*/



/*Variables*/
/*let - inside functions*/
/*var - global variable*/
/*const - constant variable*/

let myVariableNumber;

console.log(myVariableNumber);

let productName = "desktop computer";
console.log(productName);

let productPrice = 9999;
console.log(productPrice);

const interest = 3.539;
console.log(interest);

let productCode = "DC017";
const productBrand = "Dell";

console.log(productCode, productBrand);


/*JavaScript Data Types*/
let country = "Philippines";
let province = "Metro Manila";
let fullAddress = province + ", " + country;
console.log(fullAddress);

let mailAddress = province + "\n\n" + country;
console.log(mailAddress);

let message = "John's Employee";
console.log(message);

let headcount = 30;
console.log(headcount);

let grade = 90.2;
console.log(grade);

/*Exponential Notation*/
let planetDistance = 2e10;
console.log(planetDistance);

console.log("John's grade last quarter is " + grade);

let isMarried = false;
let inGoodConduct = true;

console.log("isMarried: " + isMarried);
console.log("inGoodConduct: " + inGoodConduct);

/*Arrays*/
let grades = [98.7, 92.1, 90.2, 94.6];
console.log(grades);

/*Arrays with different data types*/
let details = ['John', 'Smith', 32, true];
console.log(details);

/*Objects*/
let person = {
	fullname: "Juan Dela Cruz",
	age: 35,
	isMarried: false,
	contactNumber: ["096102301", "10312"], /*Array inside an object*/
	address:{
		housenumber: "345",   /*Nested Object*/
		city: "Manila"
	}
};

console.log(person['address']['housenumber']); /*one way of calling*/
console.log(person.address["houseNumber"]);
console.log(person.contactNumber[0]); /*another way of calling*/

let myGrades = {
	firstGrading: 98.7,
	secondGrading: 92.1,
	thirdGrading: 90.2,
	fourthGrading: 94.6
};
console.log(myGrades);

/*Return type of variable*/
console.log(typeof myGrades);
console.log(typeof grades);
console.log(typeof	message);

/*Null*/
let spouse = null;
let myNumber = 0;
let myString = "";

console.log(spouse);
console.log(myNumber);
console.log(myString);