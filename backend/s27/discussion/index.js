/*Mutator Methods*/
/*push()     - adds element after last index*/
/*pop()      - removes element from the last index */
/*unshift()  - adds element at the beginning of an array*/
/*shift() 	 - removes element at the beginning of an array*/
/*splice(index, deleteCount, elementToBeAddedFromIndex)   - removes/add from wherever inside array and add a new element based on the index paramater*/
/*sort() - arranges the elements. ASC*/
/*reverse() - arranges the elements. DESC*/

let fruits = ["Apple", "Orange", "Kiwi", "Dragon Fruit"];

console.log("Current Array");
console.log(fruits);

let fruitsLength = fruits.push("Mango");

console.log("Array after push method:");
console.log(fruits);

fruits.push("Avocado", "Guava");

console.log("Array after push method:");
console.log(fruits);


function addFruit(fruit){
	/*Push Parameter*/
	fruits.push(fruit);
}

addFruit("Pineapple");

console.log("Array after push method:");
console.log(fruits);

fruits.pop();
fruits.pop();

console.log("Array after pop method:");
console.log(fruits);

function removeFruit(){
	fruits.pop();
	console.log(fruits);
}

removeFruit();



fruits.unshift("Lime", "Banana");

console.log("Array after unshift method:");
console.log(fruits);

function UnshiftFruit(fruit){
	fruits.unshift(fruit);
	console.log(fruits);
}

UnshiftFruit("Calamansi");


fruits.shift();

console.log("Array after shift method:");
console.log(fruits);

function ShiftFruit(){
	fruits.shift();
	console.log(fruits);
}

ShiftFruit();

fruits.splice(1, 2, "Avocado");

console.log("Array after splice method:");
console.log(fruits);

function spliceFruit(index, numOfDeletionsStartingFromIndex, ToBeAdded){
	fruits.splice(index, numOfDeletionsStartingFromIndex, ToBeAdded);
	console.log(fruits);
}


spliceFruit(1, 1, "Cherry");
spliceFruit(2, 1, "Durian");

fruits.sort();

console.log("Array after sort method:");
console.log(fruits);

fruits.reverse();

console.log("Array after reverse method:");
console.log(fruits);

fruits.splice(1,1);

console.log(fruits);