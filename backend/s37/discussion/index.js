/*
    node.js
    Client-Server Architecture

    Client Applications -> Browser
    DB Servers          -> Systems developed using node.js
    DB                  -> MongoDB

    Benefits
        -> Optimized for WebApps
        -> Familiarity - JavaScript
        -> NPM - Node Package Manager
*/

/*require() directive to load node.js*/
/*http module contains the components needed to create a server*/
let server = require('http');
server.createServer((request, response) => {
    /*writeHead() -> sets the status code for response*/
    /*status 200 means OK and 404 means error*/
    response.writeHead(200, {"Content-Type": "text/plain"});
    
    /*Sends the message with text*/
    response.end("Hello World");


}).listen(4000);

/*Outputs will be shown in the gitbash terminal*/
console.log("Server is running at localhost:4000");