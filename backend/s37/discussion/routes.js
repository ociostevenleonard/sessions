let server = require('http');
let port = 4001;
let app = server.createServer((req, res) => {
	if(req.url == '/greeting'){
		res.writeHead(200, {
			"Content-Type" : "text/plain"
		});
		res.end("Hello Again");
	}else if(req.url == '/homepage'){
		res.writeHead(200, {
			"Content-Type" : "text/plain"
		});
		res.end("This is the homepage");
	}else {
		res.writeHead(404, {
			"Content-Type" : "text/plain"
		});
		res.end("404: Page not found");
	}
});

app.listen(port);
console.log(`Server now accessible at localhost:${port}`)