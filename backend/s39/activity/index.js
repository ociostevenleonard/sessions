//Note: don't add a semicolon at the end of then().
//Fetch answers must be inside the await () for each function.
//Each function will be used to check the fetch answers.
//Don't console log the result/json, return it.

// Get Single To Do [Sample]
async function getSingleToDo(){

   /*return await (
      
   );*/

}



// Getting all to do list item
async function getAllToDo(){

   return await (
      fetch('https://jsonplaceholder.typicode.com/todos')
         .then((response) => {
            return response.json();
         })
         .then((jsonObject) => {
            let newArray = jsonObject.map((ArrayElement) => ArrayElement.title);
            //console.log(newArray);
            return newArray;
            
         })         
   );

}
//getAllToDo();

// [Section] Getting a specific to do list item
async function getSpecificToDo(){
   
   return await (
      fetch('https://jsonplaceholder.typicode.com/todos/1', {
   method: 'GET'
      })
         .then((response) => {
            return response.json();
         })
         .then((jsonObject) => {
            //console.log(jsonObject);
            return jsonObject;
         })
   );

}
//getSpecificToDo();

// [Section] Creating a to do list item using POST method
async function createToDo(){
   
   return await (
      fetch('https://jsonplaceholder.typicode.com/todos', {
         method: 'POST',
         headers: {
            "Content-Type" : "application/json"
         },
         body: JSON.stringify({
            userId: 1,
            title: "Created To Do List Item",
            completed: false
         })
      })
         .then((response) => {
            return response.json();
         })
         .then((jsonObject) => {
            //console.log(jsonObject);
            return jsonObject;
         })
   );

}
//createToDo();


// [Section] Updating a to do list item using PUT method
async function updateToDo(){
   
   return await (
         fetch('https://jsonplaceholder.typicode.com/todos/1', {
            method: 'PUT',
            headers: {
               "Content-Type" : "application/json"
            },
            body: JSON.stringify({
               title : "Updated To Do List",
               description: "Updated to do list with different data structure",
               status: "Pending",
               datecompleted: "Pending",
               userId: 1
            })
         })
            .then((response) => {
               return response.json();
            })
            .then((jsonObject) => {
               //console.log(jsonObject);
               return jsonObject;
            })
      );

}
//updateToDo();
// [Section] Deleting a to do list item
async function deleteToDo(){
   
   return await (
      fetch('https://jsonplaceholder.typicode.com/todos/1', {
         method: 'DELETE'
      })
   );

}
//deleteToDo();



//Do not modify
//For exporting to test.js
try{
   module.exports = {
       getSingleToDo: typeof getSingleToDo !== 'undefined' ? getSingleToDo : null,
       getAllToDo: typeof getAllToDo !== 'undefined' ? getAllToDo : null,
       getSpecificToDo: typeof getSpecificToDo !== 'undefined' ? getSpecificToDo : null,
       createToDo: typeof createToDo !== 'undefined' ? createToDo : null,
       updateToDo: typeof updateToDo !== 'undefined' ? updateToDo : null,
       deleteToDo: typeof deleteToDo !== 'undefined' ? deleteToDo : null,
   }
} catch(err){

}