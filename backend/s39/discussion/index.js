/*
	JS Synchronous vs Asynchronous
	By Default -> JS is Synchronous -> One Statement at a time
	
*/

console.log("Hello World!");
//cosole.log("Hello Again!");
console.log("Goodbye!");


console.log("Hello World!");
/*
	for(let index = 0 ; index <= 1500; index++){
	console.log(index);
}*/
console.log("Hello Again!");

/*
	[SECTION] Getting all posts
	1. fetch("URL")
	2. fetch("URL")
		.then((response) => { return responce.json(); })
	3. fetch("URL")
		.then((response) => { return responce.json(); })
		.then((jsonObject) => { Line of Codes })
*/
fetch('https://jsonplaceholder.typicode.com/posts')
	.then((response) => {
		console.log(response.status);
		return response.json();
	})
	.then((jsonObject) => {
		console.log(jsonObject);
	});



/*
	'async' and 'await' is another approach that can be used to achieve asychronous code.
*/

async function fetchData(){
	let result = await fetch('https://jsonplaceholder.typicode.com/posts');
	console.log(result);
	console.log(typeof result);

	/*To access the response of the server by calling .body*/
	console.log(result.body);

	let json = result.json();
	console.log(json);
}	

fetchData();

/*
	[SECTION] Getting a specific post
*/
fetch('https://jsonplaceholder.typicode.com/posts/1')
	.then((response) => {
		console.log(response.status);
		return response.json();
	})
	.then((jsonObject) => {
		console.log(jsonObject);
	});



/*
	[SECTION] Creating a post
	fetch('URL', options)
		.then((response) => { return responce.json(); })
		.then((jsonObject) => { Line of Codes })
*/
fetch('https://jsonplaceholder.typicode.com/posts', {
	method: 'POST',
	headers: {
		"Content-Type" : "application/json"
	},
	body: JSON.stringify({
		title: "New Post",
		body: "Hello World!",
		userId: "1"
	})
})
	.then((response) => {
		return response.json();
	})
	.then((jsonObject) => {
		console.log(jsonObject);
	});

/*
	[SECTION] Updating a post
	fetch('URL', options)
		.then((response) => { return responce.json(); })
		.then((jsonObject) => { Line of Codes })
*/
fetch('https://jsonplaceholder.typicode.com/posts/1', {
	method: 'PUT',
	headers: {
		"Content-Type" : "application/json"
	},
	body: JSON.stringify({
		title: "Updated Post",
		body: "Hello Again!",
		userId: "1"
	})
})
	.then((response) => {
		return response.json();
	})
	.then((jsonObject) => {
		console.log(jsonObject);
	});

/*
	[SECTION] Deleting a post
	fetch('URL', options)
		.then((response) => { return responce.json(); })
		.then((jsonObject) => { Line of Codes })
*/
fetch('https://jsonplaceholder.typicode.com/posts/1', {
	method: 'DELETE'
});