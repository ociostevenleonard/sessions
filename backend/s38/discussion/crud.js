let port = 4001;
let server = require('http');

/*Mock Database*/
let directory = [
		{
			"name": "Brandon",
			"email": "brandon@mail.com"
		},
		{
			"name": "Joebert",
			"email": "joebert@mail.com"	
		}
	];
server.createServer((req, res) => {
	if (req.url == '/users' && req.method == 'GET') {
		res.writeHead(201, {'Content-type' : 'application/json'});
		res.write(JSON.stringify(directory));
		res.end();
	}else if(req.url == '/users' && req.method == 'POST'){
		/*Stores raw data into a string*/
		let reqBody = "";

		/*Gets data from user to be stores on reqBody as a String*/
		req.on('data', (dataParam) => {
			reqBody += dataParam;
		});
			

		req.on('end', () => {
			reqBody = JSON.parse(reqBody);
			let newUser = {
				"name": reqBody.name,
				"email": reqBody.email
			}

			directory.push(newUser);

			res.writeHead(200, {'Content-Type': 'application/json'});
			res.write(JSON.stringify(newUser));
			res.end();
		})
	}
}).listen(port, () => {
	console.log('Server is running at localhost:' + port);
});