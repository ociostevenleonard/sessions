let grades = [98, 94, 99, 90];
let city1 = "Tokyo", city2 = "Manila", city3 = "Jakarta";
let cityArray = [city1, city2, city3];


console.log(cityArray);
console.log(cityArray.length);

let blankArr = [];
console.log(blankArr);
console.log(blankArr.length);

let fullname = "Jamie Noble";
console.log(fullname.length);

let myTasks = [
    'drink html',
    'eat javascript',
    'inhale css',
    'bake sass'
];

console.log(myTasks);
console.log(myTasks.length);


/*to remove last array index*/
myTasks.length -= 1;

console.log(myTasks);
console.log(myTasks.length);

myTasks.splice(1);
console.log(myTasks);

/*add element*/
let theBettles = ["John", "Paul", "Ringo", "George"];
theBettles.length++;
console.log(theBettles);
theBettles[4] = "testing";
console.log(theBettles);
theBettles.push("hahha");
console.log(theBettles);

/*edit element inside array using index assignment*/
theBettles[1] = "Steven";
console.log(theBettles);


/*Accessing the last index*/
let lakersLegends = [
	"Kobe",
	"Shaq",
	"LeBron",
	"Magic",
	"Kareem"
];

let lastIndex = lakersLegends.length - 1;
console.log(lakersLegends[lastIndex]);

lakersLegends[lakersLegends.length] = "HAHAHA";
console.log(lakersLegends);

let newArr = [];

newArr[0] = "Cloud Strife";
newArr[1] = "Tifa Lockhart";
newArr[newArr.length] = "Wallace";

newArr.push("test");
console.log(newArr);

for(let index = 0 ;  index < newArr.length ; index++){
	console.log(newArr[index]);
}

let nums = [5, 12, 30, 46, 40];
for(let index = 0; index < nums.length; index++){
	if(nums[index] % 5 === 0){
		console.log(nums[index] + " is divisible by 5");
	}else{
		console.log(nums[index] + " is not divisible by 5");
	}
}