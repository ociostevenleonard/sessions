let x = 3;
let y = 2;

let sum = x + y;
console.log("Result from addition operator: " + sum);

let difference = x - y;
console.log("Result from subtraction operator: " + difference);

let product = x * y;
console.log("Result from multiplication operator: " + product);

let quotient = x / y;
console.log("Result from division operator: " + quotient);

let remainder = x % y;
console.log("Result from modulo operator: " + remainder);


// Assignment Operator
let assignmentNumber = 8;
assignmentNumber = assignmentNumber + 2;
console.log("Addition assignment: " + assignmentNumber);
assignmentNumber += 2;
console.log("Addition assignment: " + assignmentNumber);
assignmentNumber -= 2;
console.log("Subraction assignment: " + assignmentNumber);
assignmentNumber *= 2;
console.log("Multiplication assignment: " + assignmentNumber);
assignmentNumber /= 2;
console.log("Division assignment: " + assignmentNumber);




/*Increment and Decrement*/
let z = 1;

let increment = ++z;
console.log("Pre-increment: " + increment);
console.log("Pre-increment: " + z);


/*post-increment*/
increment = z++;
console.log("Post-increment: " + increment);

let myNum1 = 1, myNum2 = 1;
let preIncrement = ++myNum1;
preIncrement = ++myNum1;
console.log(preIncrement);

let postIncrement = myNum2++;
console.log(postIncrement);

/*Type Coercion*/
let myNum3 = "10";
let myNum4 = 12;

let coercion = myNum3 + myNum4;
console.log(coercion);

let myNum5 = true + 1;
console.log(myNum5);


/*Comparison Operator*/
let juan = "Juan";


/*Equality Op*/
console.log(juan == "Juan");

console.log(1 == 2);

console.log(false == 0);

/*Inequality Op*/
console.log(juan != "Juan");

console.log(1 != 2);

console.log(false != 0);

/*Strict Equality Operator*/
console.log(1 === 1); /*Used to determine if they have the same datatype and same content*/
console.log(false === 0);

/*Strict Inequality Operator*/
console.log(1 !== 1); /*Used to determine if they have the same datatype and same content*/
console.log(false !== 0);

let a = 50;
let b = 65;
console.log(a > b);
console.log(a < b);

let example = a >= b;
console.log(example);

let isLegalAge = true;
let isRegistered = true;

let allReqsMet = isLegalAge && isRegistered;

console.log("Logical AND: " + allReqsMet);


let isLegalAge2 = true;
let isRegistered2 = false;
let someReqsMet = (isLegalAge2 || isRegistered2);
console.log("Logical OR: " + someReqsMet);


let isRegistered3 = false;
let someReqsNotMet = !isRegistered3;
console.log("Logical NOT: " + someReqsNotMet);