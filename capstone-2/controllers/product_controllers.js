const User = require('../models/User.js');
const Product = require('../models/Product.js');
const Order = require('../models/Order.js');
const auth = require('../auth.js');
const bcrypt = require('bcrypt');
const cloudinary = require('../utils/cloudinary.js');
var mongoose = require('mongoose');



/*Create Product*/
module.exports.CreateProduct = (request, response, next) => {
	let errorMsg = [];
	let create_name = request.body.name;
	let create_description = request.body.description;
	let create_price = request.body.price;
	let create_unit = request.body.unit;
	let image_base64 = request.body.image;

	if(create_name === null || create_name === undefined || create_name === ""){
		errorMsg.push("Product Name is Empty");
	}

	if(create_description === null || create_description === undefined || create_description === ""){
		errorMsg.push("Description is Empty");
	}

	if(create_price === null || create_price === undefined || create_price === ""){
		errorMsg.push("Price is Empty");
	}

	if(create_unit === null || create_unit === undefined || create_unit === ""){
		errorMsg.push("Unit is Empty");
	}

	if(errorMsg.length < 1){
		let imageInfo = null;
		cloudinary.uploader.upload(image_base64, {
				folder: 'greystar',
				width: 300,
				crop: 'scale',
				resource_type: "image"
			}).then((result, error) => {
				if(error){
					console.log("Image Upload Error");
					console.log(error);
					return response.send({
						isOk: false,
						message: error
					})
				}else{
					console.log(result.url);
					let newProduct = new Product({
						image: {
							public_id: result.public_id,
							url: result.url
						},
						name: create_name,
						description: create_description,
						price: create_price,
						unit: create_unit
					});
					return newProduct.save().then((savedTask, saveErr) => {
						if(saveErr){
							return response.send({
								isOk: false,
								message: `Product not saved! Please try again.`
							});
						}else{
							return response.send({
								isOk: true,
								message: `${request.body.name} is successfully saved.`
							});
						}
					}).catch(err => err);
				}
			}).catch(err => console.log(err));
	}else{
		return response.send(errorMsg);
	}
};

/*Check if product is already registered*/
module.exports.checkIfProductIsRegistered = (request, response, next) => {
	return Product.find({
			name: request.body.name
		}).then((result, error) => {
			if(result.length < 1){
				next();
			}else{
				return response.send({
					isOk: false,
					message: `${request.body.name} is already registered.`
				});
			}
		});
};

/*Show All Products UNFINISED*/
module.exports.ShowAllProducts = async (request, response) => {
	let ShowItems = await Product.find({}).then((result, error) => {
			if(result.length < 1){
				return response.send(`No Products Registered`);
			}else{
				let temporaryArrayHolder = [];
				/*let StockPurchasedHolder = Order.find({}).then((result) => {

				});*/






				result.forEach((Element) => {
					let totalStock = 0, totalTrashed = 0;
					Element.stocks.forEach((items) => {
							totalStock += items.quantity;
						});
					Element.trashed_stocks.forEach((items) => {
							totalTrashed += items.quantity;
						});

					temporaryArrayHolder.push({
						image: Element.image,
						_id: Element._id,
						name: Element.name,
						description: Element.description,
						price: Element.price,
						isActive: Element.isActive,
						unit: Element.unit,
						createdOn: Element.createdOn,
						availableStock: (totalStock - totalTrashed)
					});
				});

				return temporaryArrayHolder;
			}
		});

	let GetAllOrders = await Order.find({}).then((result) => {
		return result;
	});

	/*Updating Stock Viewing*/
	ShowItems.forEach((ShowItems_item) => {
		/*Checking*/
		GetAllOrders.forEach((item) => {
			let total_Holder = 0;
			//console.log(item.productsPurchased);
			let OrderItems = item.productsPurchased
			OrderItems.forEach((item_item) => {
				if(item_item.productID == ShowItems_item._id){
					total_Holder += item_item.quantity;
				}
			});
			ShowItems_item.availableStock = (ShowItems_item.availableStock - total_Holder);
		});



	})

	return response.send(ShowItems);
};

/*Show All Active Products*/
module.exports.ShowAllActiveProducts = async (request, response) => {
	let ShowItems = await Product.find({
		isActive: true
	}).then((result, error) => {
			if(result.length < 1){
				return response.send(`No Products Registered`);
			}else{
				let temporaryArrayHolder = [];
				/*let StockPurchasedHolder = Order.find({}).then((result) => {

				});*/






				result.forEach((Element) => {
					let totalStock = 0, totalTrashed = 0;
					Element.stocks.forEach((items) => {
							totalStock += items.quantity;
						});
					Element.trashed_stocks.forEach((items) => {
							totalTrashed += items.quantity;
						});

					temporaryArrayHolder.push({
						image: Element.image,
						_id: Element._id,
						name: Element.name,
						description: Element.description,
						price: Element.price,
						isActive: Element.isActive,
						unit: Element.unit,
						createdOn: Element.createdOn,
						availableStock: (totalStock - totalTrashed)
					});
				});

				return temporaryArrayHolder;
			}
		});

	let GetAllOrders = await Order.find({}).then((result) => {
		return result;
	});

	/*Updating Stock Viewing*/
	ShowItems.forEach((ShowItems_item) => {
		/*Checking*/
		GetAllOrders.forEach((item) => {
			let total_Holder = 0;
			//console.log(item.productsPurchased);
			let OrderItems = item.productsPurchased
			OrderItems.forEach((item_item) => {
				if(item_item.productID == ShowItems_item._id){
					total_Holder += item_item.quantity;
				}
			});
			ShowItems_item.availableStock = (ShowItems_item.availableStock - total_Holder);
		});



	})

	return response.send(ShowItems);
};

/*Show Single Products*/
module.exports.ShowSingleProduct = async (request, response) => {
	let SingleProduct = await Product.findById(request.params.prodID).then((result, error) => {
			if(result === null){
				return response.send(`No Product Registered`);
			}else{
				let temporaryArrayHolder = [];
				let totalStock = 0, totalTrashed = 0;

				result.stocks.forEach((items) => {
						totalStock += items.quantity;
					});
				result.trashed_stocks.forEach((items) => {
						totalTrashed += items.quantity;
					});

				temporaryArrayHolder.push();


				return {
						image: result.image,
						_id: result._id,
						name: result.name,
						description: result.description,
						price: result.price,
						isActive: result.isActive,
						unit: result.unit,
						createdOn: result.createdOn,
						availableStock: totalStock - totalTrashed
					};
			}
		});

	let GetAllOrders = await Order.find({}).then((result) => {
		return result;
	});

	GetAllOrders.forEach((item) => {
		let total_Holder = 0;
		let OrderItems = item.productsPurchased
		OrderItems.forEach((item_item) => {
			if(item_item.productID == SingleProduct._id){
				total_Holder += item_item.quantity;
			}
		});
		SingleProduct.availableStock = (SingleProduct.availableStock - total_Holder);
	});

	return response.send(SingleProduct);
};

/*Add Stock Product*/
module.exports.AddStock = (request, response) => {
	let errorMsg = [];
	let create_stock = request.params.prodID;	
	let stock = request.body.quantity;
	let deliveredBy = request.body.deliveredBy;

	if(stock === null || stock === undefined || stock === ""){
		errorMsg.push({
			isOk: false,
			message: "Stock is Empty"
		});
	}

	if(deliveredBy === null || deliveredBy === undefined || deliveredBy === ""){
		errorMsg.push({
			isOk: false,
			message: "Delivered By is Empty"
		});
	}

	if(errorMsg.length < 1){
		return Product.findById(create_stock).then((result) => {
					let newStock = {
						quantity: request.body.quantity,
						deliveredBy: request.body.deliveredBy
					};
					
					result.stocks.push(newStock);

					return result.save().then((result, error) => {
						if(error){
							return response.send({
								isOk: false,
								message: error
							});
						}else{
							return response.send({
								isOk: true,
								message: `Added ${request.body.quantity} stock(s) to Product ID ${create_stock}.`
							});
						}
					}).catch(err => response.send(err));
				});
	}else{
		return response.send(errorMsg);
	}
};

/*Remove Stock Product*/
module.exports.RemoveStock = (request, response) => {
	let errorMsg = [];
	let remove_stock = request.params.prodID;	
	let stock = request.body.quantity;
	let res = request.body.reasonForTrashing;

	if(stock === null || stock === undefined || stock === ""){
		errorMsg.push({
			isOk: false,
			message: "Stock is Empty"
		});
	}

	if(res === null || res === undefined || res === ""){
		errorMsg.push({
			isOk: false,
			message: "Reason is Empty"
		});
	}

	if(errorMsg.length < 1){
		return Product.findById(remove_stock).then((result) => {
					let newStock = {
						quantity: request.body.quantity,
						reasonForTrashing: request.body.reasonForTrashing
					};

					result.trashed_stocks.push(newStock);

					return result.save().then((result, error) => {
						if(error){
							return response.send({
								isOk: false,
								message: error
							});
						}else{
							return response.send({
								isOk: true,
								message: `Removed ${request.body.quantity} stock(s) to Product ID ${remove_stock}.`
							});
						}
					}).catch(err => response.send(err));
				});
	}else{
		return response.send(errorMsg);
	}
};

/*Update Product Info*/
module.exports.updateProductInfo = (request, response) => {
	const h_name = request.body.name;
	const h_description = request.body.description;
	const h_price = request.body.price;
	const h_unit = request.body.unit;

	const productID = request.params.prodID;

	return Product.findByIdAndUpdate(productID, {
		name: h_name,
		description: h_description,
		price: h_price,
		unit: h_unit
	}).then((savedTask, saveErr) => {
		if(saveErr){
			return response.send({
								isOk: false,
								message: saveErr
							});
		}else{
			return response.send({
								isOk: true,
								message: `${productID}'s information was successfully updated.`
							});
		}
	}).catch(err => err);
};

/*Check if product is already registered from Params*/
module.exports.checkIfProductIsRegisteredFromParams = (request, response, next) => {
	return Product.find({
			$and: [
					{
						_id: {
							$ne: request.params.prodID 
						}
					},
					{
						name: request.body.name
					}
				]
		}).then((result, error) => {
			if(result.length < 1){
				next();
			}else{
				return response.send({
								isOk: false,
								message: `${request.body.name} is already registered.`
							});
			}
		});
};


/*Archive Product Info*/
module.exports.ArchiveProduct = (request, response) => {
	const productID = request.params.prodID;

	return Product.findByIdAndUpdate(productID, {
		isActive: false
	}).then((savedTask, saveErr) => {
		if(saveErr){
			return response.send({
				isOk: false,
				message: `Product not updated! Please try again.`
			});
		}else{
			return response.send({
				isOk: true,
				message: `${productID} has been successfully acrchived.`
			});
		}
	}).catch(err => err);
};


/*Activate Product Info*/
module.exports.ActivateProduct = (request, response) => {
	const productID = request.params.prodID;
	
	return Product.findByIdAndUpdate(productID, {
		isActive: true
	}).then((savedTask, saveErr) => {
		if(saveErr){
			return response.send({
				isOk: false,
				message: `Product not updated! Please try again.`
			});
		}else{
			return response.send({
				isOk: true,
				message: `${productID} has been successfully activated.`
			});
		}
	}).catch(err => err);
};

/*Deliveries*/
module.exports.ShowDeliveries = (request, response) => {
	let productId = request.params.prodID;

	return Product.findById(productId).then((result) => {
		return response.send(result.stocks);
	});
};

/*Show Trashed Stock*/
module.exports.ShowTrashed = (request, response) => {
	let productId = request.params.prodID;

	return Product.findById(productId).then((result) => {
		return response.send(result.trashed_stocks);
	});
};



module.exports.SearchProductByName = async (request, response) => {
	let ShowItems = await Product.find({
		name: { 
			$regex: request.body.searchBar, 
			$options: 'i' 
		},
		isActive: request.body.isActive
	}).sort({
		name: 1
	}).then((result, error) => {
			if(result==null){
				return response.send([]);
			}else{
				let temporaryArrayHolder = [];
				/*let StockPurchasedHolder = Order.find({}).then((result) => {

				});*/






				result.forEach((Element) => {
					let totalStock = 0, totalTrashed = 0;
					Element.stocks.forEach((items) => {
							totalStock += items.quantity;
						});
					Element.trashed_stocks.forEach((items) => {
							totalTrashed += items.quantity;
						});

					temporaryArrayHolder.push({
						image: Element.image,
						_id: Element._id,
						name: Element.name,
						description: Element.description,
						price: Element.price,
						isActive: Element.isActive,
						unit: Element.unit,
						createdOn: Element.createdOn,
						availableStock: (totalStock - totalTrashed)
					});
				});

				return temporaryArrayHolder;
			}
		});

	let GetAllOrders = await Order.find({}).then((result) => {
		return result;
	});
		ShowItems.forEach((ShowItems_item) => {
			GetAllOrders.forEach((item) => {
				let total_Holder = 0;
				let OrderItems = item.productsPurchased
				OrderItems.forEach((item_item) => {
					if(item_item.productID == ShowItems_item._id){
						total_Holder += item_item.quantity;
					}
				});
				ShowItems_item.availableStock = (ShowItems_item.availableStock - total_Holder);
			});
		})
	return response.send(ShowItems);
};

module.exports.SearchProductByNameAdmin = async (request, response) => {
	let ShowItems = await Product.find({
		name: { 
			$regex: request.body.searchBar, 
			$options: 'i' 
		},
	}).sort({
		name: 1
	}).then((result, error) => {
			if(result==null){
				return response.send([]);
			}else{
				let temporaryArrayHolder = [];
				/*let StockPurchasedHolder = Order.find({}).then((result) => {

				});*/






				result.forEach((Element) => {
					let totalStock = 0, totalTrashed = 0;
					Element.stocks.forEach((items) => {
							totalStock += items.quantity;
						});
					Element.trashed_stocks.forEach((items) => {
							totalTrashed += items.quantity;
						});

					temporaryArrayHolder.push({
						image: Element.image,
						_id: Element._id,
						name: Element.name,
						description: Element.description,
						price: Element.price,
						isActive: Element.isActive,
						unit: Element.unit,
						createdOn: Element.createdOn,
						availableStock: (totalStock - totalTrashed)
					});
				});

				return temporaryArrayHolder;
			}
		});

	let GetAllOrders = await Order.find({}).then((result) => {
		return result;
	});
		ShowItems.forEach((ShowItems_item) => {
			GetAllOrders.forEach((item) => {
				let total_Holder = 0;
				let OrderItems = item.productsPurchased
				OrderItems.forEach((item_item) => {
					if(item_item.productID == ShowItems_item._id){
						total_Holder += item_item.quantity;
					}
				});
				ShowItems_item.availableStock = (ShowItems_item.availableStock - total_Holder);
			});
		})
	return response.send(ShowItems);
};




/*Update Product Info*/
module.exports.UpdateProductImage = (request, response) => {
	const new_image = request.body.image;
	const old_image_link = request.body.old_image_link

	const productID = request.params.prodID;
	cloudinary.uploader.destroy(old_image_link).then(result => {
		cloudinary.uploader.upload(new_image, {
				folder: 'greystar',
				width: 300,
				crop: 'scale',
				resource_type: "image"
			}).then((result, error) => {
				if(error){
					return response.send({
						isOk: false,
						message: error
					})
				}else{
					return Product.findByIdAndUpdate(productID, {
							image: {
								public_id: result.public_id,
								url: result.url
							}
						}).then((savedTask, saveErr) => {
							if(saveErr){
								return response.send({
													isOk: false,
													message: saveErr
												});
							}else{
								return response.send({
													isOk: true,
													message: `${productID}'s image was successfully updated.`
												});
							}
						}).catch(err => err);
				}
			}).catch(err => console.log(err));
	})
};




module.exports.ShowSoldTotal = (request, response) => {
	let productId = request.params.prodID;
	return Order.find({}).then((result) => {
		let totalHolder = 0;
		let data = [];
		result.forEach(order => {
			order.productsPurchased.forEach(purchasedProduct => {
				if(purchasedProduct.productID == productId){
					totalHolder += purchasedProduct.quantity;
					data.push({
						orderId: order._id,
						quantity: purchasedProduct.quantity,
						price: purchasedProduct.price
					})
				}
			})
		});


		return response.send({
			total: totalHolder,
			data: data
		});
	});
};