const express = require("express");
const router = express.Router();
const productController = require("../controllers/product_controllers.js");
const auth = require('../auth.js');
const bcrypt = require('bcrypt');

const {verify, verifyAdmin} = auth;

/*Create Product*/
router.post('/newProduct',verify, verifyAdmin, productController.checkIfProductIsRegistered, productController.CreateProduct);

/*Show All Product*/
router.get('/showAll',verify, verifyAdmin, productController.ShowAllProducts);

/*Show All Active Product*/
router.get('/showAllActive', productController.ShowAllActiveProducts);

/*Show Single Product*/
router.get('/getproduct/:prodID', productController.ShowSingleProduct);

/*Add Stock*/
router.post('/addStock/:prodID',verify, verifyAdmin, productController.AddStock);

/*Remove Stock*/
router.post('/removeStock/:prodID',verify, verifyAdmin, productController.RemoveStock);

/*Update Product*/
router.put('/updateInfo/:prodID',verify, verifyAdmin, productController.checkIfProductIsRegisteredFromParams, productController.updateProductInfo);

/*Archive Product*/
router.put('/:prodID/archive',verify, verifyAdmin, productController.ArchiveProduct);

/*Activate Product*/
router.put('/:prodID/activate',verify, verifyAdmin, productController.ActivateProduct);

/*Show Deliveries*/
router.get('/showDeliveries/:prodID', verify, verifyAdmin, productController.ShowDeliveries);
module.exports = router;
/*Show Deliveries*/
router.get('/showTrash/:prodID', verify, verifyAdmin, productController.ShowTrashed);

router.post('/SearchProductByName', productController.SearchProductByName);

router.post('/SearchProductByNameAdmin', productController.SearchProductByNameAdmin);

router.put('/UpdateProductImage/:prodID', productController.UpdateProductImage);

router.get('/ShowSoldTotal/:prodID', verify, verifyAdmin, productController.ShowSoldTotal)

module.exports = router;