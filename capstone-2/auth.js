/*Dependencies and Modules*/
const jwt = require('jsonwebtoken');
const secret = "CinderellChicken";

/*Token Creation*/
module.exports.createAccessToken = (user) => {
	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	}
	return jwt.sign(data, secret, {});
}




/*to check if user has token ID*/
module.exports.verify = (request, response, next) => {
	let token = request.headers.authorization;

	if(typeof token === "undefined"){
		return response.send({
			auth: "Failed, No Token"
		});
	}else{
		token = token.substring(7, token.length);
		jwt.verify(token, secret, (error, decodedToken) => {
			if(error){
				return response.send({
					isOk: "Failed",
					message: error.message
				});
			}else{
				request.user = decodedToken;

				next();
			}
		});
	}
};


module.exports.verifyAdmin = (req, res, next) => {
  if (req.user.isAdmin) {
    next();
  } else {
    return res.send({
      auth: `Failed`,
      message: `Action Forbidden`,
    });
  }
};