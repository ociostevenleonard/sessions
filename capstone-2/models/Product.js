const mongoose = require("mongoose");

module.exports = mongoose.model("Product", new mongoose.Schema({
	image: {
		public_id: {
			type: String,
			required: [true, 'Image is required']
		},
		url: {
			type: String,
			required: [true, 'Image URL is required']
		}
	},
	name: {
		type: String,
		required: [true, 'Product Name is required!']
	},
	description: {
		type: String,
		required: [true, 'Description is required!']
	},
	price: {
		type: Number,
		required: [true, 'Price is required!']
	},
	isActive: {
		type: Boolean,
		default: true
	},
	unit: {
		type: String,
		required: [true, 'Unit is required!']
	},
	createdOn: {
		type: Date,
		default: new Date()
	},
	stocks: [
		{
			quantity: {
				type: Number,
				required: [true, 'Qty is required!']
			},
			deliveredBy: {
				type: String,
				required: [true, 'Delivered By is required!']
			},
			deliveredOn: {
					type: Date,
					default: new Date()
			},
		}
	],
	trashed_stocks: [
		{
			quantity: {
				type: Number,
				required: [true, 'Qty is required!']
			},
			reasonForTrashing: {
				type: String,
				required: [true, 'Reason is required!']
			},
			trashedOn: {
					type: Date,
					default: new Date()
			},
		}
	]

}));