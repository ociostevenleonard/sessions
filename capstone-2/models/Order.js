const mongoose = require("mongoose");

module.exports = mongoose.model("Order", new mongoose.Schema({
	userId: {
		type: String,
		required: [true, 'User ID is required!']
	},
	conversation: [
		{
			fromSender: {
				type: String,
				required: [true, 'Product ID is required!']
			},
			message: {
				type: String,
				required: [true, 'Message is required!']
			},
			sentOn: {
				type: Date,
				default: new Date()
			},
			isActive: {
				type: Boolean,
				default: true
			}
		}
	],
	productsPurchased: [
		{
			productID: {
				type: String,
				required: [true, 'Product ID is required!']
			},
			quantity: {
				type: Number,
				required: [true, 'Qty is required!']
			},
			price: {
				type: Number,
				required: [true, 'Price is required!']
			}
		}
	],
	actionsTaken: [
		{
			action: {
				type: String,
				required: [true, 'action is required!']
			},
			action_date: {
				type: Date,
				default: new Date()
			},
			remarks: {
				type: String,
				required: [true, 'remarks is required!']
			}
		}
	]
}));