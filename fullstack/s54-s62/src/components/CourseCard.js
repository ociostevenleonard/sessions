import {Card, Button} from 'react-bootstrap';
import PropTypes from 'prop-types';
import {useState} from 'react';

/*import coursesDatabase from '../data/coursesDatabase.js';*/

export default function CourseCard({courseProp}){
	const {name, description, price} = courseProp;

	/*
	Use the state hook in this component to be able to store its state
		Syntax
			const [getter, setter] = useState(initialGetterValue);

	*/

	const [count, setCount] = useState(0);
	const [seatCount, setSeatCount] = useState(30);

	/*function enroll(){
		if(seatCount > 0){
			setCount(count + 1);
			setSeatCount(seatCount - 1);
		}else{
			alert("No more seats available.");
		}
	}*/

	return (
		<Card id='courseComponent1' className='cardHighlight p-3 mb-2'>
			<Card.Body>
				<Card.Title>
					{name}
				</Card.Title>
				<Card.Subtitle>
					Description:
				</Card.Subtitle>
				<Card.Text>
				  	{description}
				</Card.Text>
				<Card.Subtitle>
					Price:
				</Card.Subtitle>
				<Card.Text>
				  	PhP {price}
				</Card.Text>
				<Card.Text>
				  	Enrollees: {count}
				</Card.Text>
				<Button variant="primary" onClick={()=>{
					if(seatCount > 0){
						setCount(count + 1);
						setSeatCount(seatCount - 1);
					}else{
						alert("No more seats available.");
					}
				}} >ENROLL</Button>		
			</Card.Body>
		</Card>	
	);
}


// Checks if the CourseCard component is getting the correct prop types.
CourseCard.propTypes = {
	course: PropTypes.shape({
		name: PropTypes.string.isRequired,
		description: PropTypes.string.isRequired,
		price: PropTypes.number.isRequired
	})
};