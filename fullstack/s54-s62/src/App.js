import './App.css';
import AppNavbar from './components/AppNavbar.js';
import Home from './pages/Home.js';
import Courses from './pages/Courses.js';
import Register from './pages/Register.js';
import Login from './pages/Login.js';

import { Container } from 'react-bootstrap';


export default function App() {
	return (
		<>
			<AppNavbar />
			<Container>
				{/*<Home />
				<Courses />
				<Register />*/}
				<Login />
			</Container>
		</>
  	);
};
