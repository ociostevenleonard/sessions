import {Row, Col, Card, Button} from 'react-bootstrap';
import CourseCard from '../components/CourseCard.js';
import coursesDatabase from '../data/coursesDatabase.js';

export default function Courses(){
	/*map is like forEach but has a return value for a new string*/
	const courses = coursesDatabase.map((course) => {
		return (
			<CourseCard key={course.id} courseProp={course}/>
		);
	});

	return (
		<>
			{courses}
		</>
	);
}