import {Form, Button} from 'react-bootstrap';
import {useState, useEffect, useContext} from 'react';

export default function Login(){
	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");

	const[isActive, setIsActive] = useState(false);

	function login(event){
		/*Prevents the default behavior during submission which is page redirection via form submission*/
		event.preventDefault();

		fetch('http://localhost:4000/users/login', {
			method: "POST",
			headers: {
				"Content-Type": "application/json"
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		}).then((result) => result.json())
		.then((data) => {
			/*response from postman*/
			if(data == false){
				alert("Unsuccessful Login");
			}else{
				setEmail('');
				setPassword('');
				console.log(data.access);
				alert("Thank you for logging in.");
			}
		});
	}

	/*Para dagdag effects*/
	/*changes everytime state changes on the indicated getters in the array.*/
	useEffect(() => {
		if(email !=="" && password !==""){
			setIsActive(true);
		}else{
			setIsActive(false);
		}
	},[email, password]);

	return (
		<Form onSubmit={(ev) => login(ev)}>
			<h3 className="my-5">Login</h3>
			<Form.Group>
				<Form.Label>Email:</Form.Label>
				<Form.Control 
					type="email" 
					placeholder="Enter Email" 
					required 
					value={email} 
					onChange={e => {
							setEmail(e.target.value);
					}}
				/>
			</Form.Group>
			<Form.Group className='mb-3'>
				<Form.Label>Password:</Form.Label>
				<Form.Control 
					type="password" 
					placeholder="Enter Password" 
					required 
					value={password} 
					onChange={e => {
							setPassword(e.target.value);
					}}
				/>
			</Form.Group>
			{/*Conditional Rendering*/}
			{
				isActive ? 
				<Button 
					variant="success" 
					type="submit" 
					className='mb-2' 
				>LOGIN</Button> 
				:
				<Button 
					variant="success" 
					type="submit" 
					className='mb-2'
					disabled 
				>LOGIN</Button>  
			}
			
		</Form>
	);
};