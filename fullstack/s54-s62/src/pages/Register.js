import {Form, Button} from 'react-bootstrap';
import {useState, useEffect, useContext} from 'react';

export default function Register(){
	const [firstName, setFirstName] = useState("");
	const [lastName, setLastName] = useState("");
	const [email, setEmail] = useState("");
	const [mobileNo, setMobileNum] = useState("");
	const [password, setPassword] = useState("");
	const [retypePassword, setRetypePassword] = useState("");
	
	const[isActive, setIsActive] = useState(false);

	function registerUser(event){
		/*Prevents the default behavior during submission which is page redirection via form submission*/
		event.preventDefault();

		fetch('http://localhost:4000/users/register', {
			method: "POST",
			headers: {
				"Content-Type": "application/json"
			},
			body: JSON.stringify({
				firstName: firstName,
				lastName: lastName,
				email: email,
				mobileNo: mobileNo,
				password: password
			})
		}).then((result) => result.json())
		.then((data) => {
			/*response from postman*/
			if(data == true){
				setFirstName('');
				setLastName('');
				setEmail('');
				setMobileNum('');
				setPassword('');
				setRetypePassword('');

				alert("Thank you for registering!");
			}else{
				alert("Something went wrong.");
			}
		});
	}

	/*Para dagdag effects*/
	/*changes everytime state changes on the indicated getters in the array.*/
	useEffect(() => {
		if((firstName !== "" && lastName !==""  && email !=="" && mobileNo !=="" && password !=="" && retypePassword !=="") && (password == retypePassword) && (mobileNo.length === 11)){
			setIsActive(true);
		}else{
			setIsActive(false);
		}
	},[firstName, lastName, email, mobileNo, password, retypePassword]);

	return (
		<Form onSubmit={(ev) => registerUser(ev)}>
			<h1 className="my-5 text-center">Register</h1>
			<Form.Group>
				<Form.Label>First Name:</Form.Label>
				<Form.Control 
					type="text" 
					placeholder="Enter First Name"
					required value={firstName}
					onChange={e => {
						setFirstName(e.target.value);
					}}
				/>
			</Form.Group>
			<Form.Group>
				<Form.Label>Last Name:</Form.Label>
				<Form.Control 
					type="text" 
					placeholder="Enter Last Name" 
					required 
					value={lastName} 
					onChange={e => {
						setLastName(e.target.value);
					}}
				/>
			</Form.Group>
			<Form.Group>
				<Form.Label>Email:</Form.Label>
				<Form.Control 
					type="email" 
					placeholder="Enter Email" 
					required 
					value={email} 
					onChange={e => {
							setEmail(e.target.value);
					}}
				/>
			</Form.Group>
			<Form.Group>
				<Form.Label>Mobile No:</Form.Label>
				<Form.Control 
					type="number" 
					placeholder="Enter 11 Digit No." 
					required 
					value={mobileNo} 
					onChange={e => {
							setMobileNum(e.target.value);
					}}
				/>
			</Form.Group>
			<Form.Group>
				<Form.Label>Password:</Form.Label>
				<Form.Control 
					type="password" 
					placeholder="Enter Password" 
					required 
					value={password} 
					onChange={e => {
							setPassword(e.target.value);
					}}
				/>
			</Form.Group>
			<Form.Group className='mb-3'>
				<Form.Label>Confirm Password:</Form.Label>
				<Form.Control 
					type="password" 
					placeholder="Confirm Password" 
					required 
					value={retypePassword} 
					onChange={e => {
							setRetypePassword(e.target.value);
					}}
				/>
			</Form.Group>
			{/*Conditional Rendering*/}
			{
				isActive ? 
				<Button 
					variant="primary" 
					type="submit" 
					className='mb-2' 
				>Submit</Button> 
				:
				<Button 
					variant="primary" 
					type="submit" 
					className='mb-2'
					disabled 
				>Submit</Button>  
			}
			
		</Form>
	);
};




