import './App.css';

import AppNavbar from './components/AppNavbar.js';
import Banner from './components/Banner.js';

import {UserProvider} from './UserContext.js';
import {useState} from 'react';

import Home from './pages/Home.js';
import Login from './pages/Login.js';
import Logout from './pages/Logout.js';
import Register from './pages/Register.js';
import Profile from './pages/Profile.js';
import Products from './pages/Products.js';
import ProductView from './pages/ProductView.js';
import MyCart from './pages/MyCart.js';
import ProductInfo from './pages/ProductInfo.js';
import UserOrders from './pages/UserOrders.js';

import { BrowserRouter as Router } from 'react-router-dom';
import {Route, Routes} from 'react-router-dom';

import { Container } from 'react-bootstrap';

function App() {
  const [user, setUser] = useState({
    access: localStorage.getItem('myToken')
  });

  const unsetUser = () => {
    localStorage.clear();
  }
  return (
    <>
    <UserProvider value = {{user, setUser, unsetUser}}>
      <Router>
        <Container fluid>
          <AppNavbar />
          <Routes>
            <Route path='/' element={<Home />} />
            <Route path='/login' element={<Login />} />
            <Route path='/logout' element={<Logout />} />
            <Route path='/register' element={<Register />} />
            <Route path='/profile' element={<Profile />} />
            <Route path='/products' element={<Products />} />
            <Route path='/productinfo/:prodID' element={<ProductInfo />} />
            <Route path='/products/:prodID' element={<ProductView />} />
            <Route path='/mycart' element={<MyCart />} />
            <Route path='/orders' element={<UserOrders />} />
            {/*
            <Route path='/*' element={<ErrorPage />} />
            <Route path='/adminView' element={<AdminView />} />*/}
          </Routes>
        </Container>
      </Router>
    </UserProvider> 
    </>
    );
}

export default App;
