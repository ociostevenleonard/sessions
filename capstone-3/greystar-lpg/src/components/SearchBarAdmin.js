import React, { useState, useEffect } from 'react';
import {Col, Row, Container, Table} from 'react-bootstrap';
import ProductCards from './ProductCards.js';
import CreateProduct from './CreateProduct.js';
import UpdateProduct from './UpdateProduct.js';
import UpdateProductImage from './UpdateProductImage.js';
import Swal from 'sweetalert2';
import {Link} from 'react-router-dom';
import BeatLoader from "react-spinners/BeatLoader";

export default function SearchBarAdmin ({productData}){
  const [searchQuery, setSearchQuery] = useState('');
  const [searchResults, setSearchResults] = useState([]);
  const [loading, setLoading] = useState(false);

  const [products, setProducts] = useState([])

  useEffect(() => {
    handleSearch(searchQuery);
  }, [searchQuery])

  const handleSearch = async (data_input) => {
    setLoading(true);
    let body = { 
        searchBar: data_input
    }
    fetch(`${process.env.REACT_APP_API_URL}/products/SearchProductByNameAdmin/`, {
    		method: "POST",
    		headers: {
				"Content-Type": "application/json"
			},
			body: JSON.stringify(body)
    	}).then((result) => result.json())
    	.then((data) => {
        setLoading(false);
    		if(data.isOk === false){
    			Swal.fire({
    			  icon: 'error',
    			  title: 'Oops...',
    			  text: data.message,
    			})
    		}else{
	    		const ProductArr = data.map(product => {
              let ProductStatus = (product.isActive) ? "Active" : "Archived";
              let ProductStatusColor = (product.isActive) ? "text-success" : "text-danger";
	    		    return (
                      <tr key={product._id}>
                        <td className='align-middle'>

                          <Link 
                              to = {'/productinfo/' + product._id}
                          >
                            <strong>{product.name}</strong>
                          </Link>

                          <div>{product._id} <div className={`${ProductStatusColor} `}><strong>{ProductStatus}</strong></div></div>
                        </td>
                        <td className='align-middle text-center'>
                        &#8369;{product.price.toFixed(2)}
                        </td>
                        <td className='align-middle text-center'>
                          {product.availableStock}
                        </td>
                      </tr>
              );
	    		})

	    		//set the courses state to the result of our map function, to bring our returned course component outside of the scope of our useEffect where our return statement below can see.
	    		setProducts(ProductArr)
    		}
    	});
  };

  function toArchive(ProductId, ProductName, action){
        Swal.fire({
          title: `${action} this item?`,
          text: `${ProductName}`,
          icon: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: `Yes, let's proceed!`
        }).then((result) => {
          if (result.isConfirmed) {
            fetch(`${process.env.REACT_APP_API_URL}/products/${ProductId}/${action}`, {
                method: "PUT",
                headers: {
                    "Content-Type": "application/json",
                    'Authorization': `Bearer ${localStorage.getItem('myToken')}`
                },
                body: JSON.stringify({
                    prodID: ProductId
                })
            }).then((result) => result.json())
            .then((data) => {
                console.log(data);
                if(data.isOk === true){
                    Swal.fire({
                      icon: 'success',
                      title: `Successfully Removed`,
                      text: data.message,
                    })
                }else{
                    Swal.fire({
                      icon: 'error',
                      title: 'Oops...',
                      text: data.message,
                    })
                }
            });
          }
        });
    }


  function stockManage(ProductId, ProductName, action, input2Placeholder){
    Swal.fire({
      title: `${action} to ${ProductName}`,
      html:
      '<input id="qty" type="number" placeholder="Quantity" class="swal2-input" required>' +
      '<input id="delBy" type="text" placeholder="'+ input2Placeholder +'" class="swal2-input" required>',
      focusConfirm: false,
      icon: 'warning',
      showCancelButton: true,
      preConfirm: () => {
        const qty = document.getElementById('qty').value;
        const deliveredBy = document.getElementById('delBy').value
        let forBody = (action == 'addStock') ? {
          quantity: qty,
          deliveredBy: deliveredBy
        } : {
          quantity: qty,
          reasonForTrashing: deliveredBy
        }
        if(qty && deliveredBy){
          fetch(`${process.env.REACT_APP_API_URL}/products/${action}/${ProductId}`, {
                          method: "POST",
                          headers: {
                              "Content-Type": "application/json",
                              'Authorization': `Bearer ${localStorage.getItem('myToken')}`
                          },
                          body: JSON.stringify(forBody)
                      }).then((result) => result.json())
                      .then((data) => {
                          console.log(data)
                          if(data.isOk === true){
                              Swal.fire({
                                icon: 'success',
                                title: `Successfully Removed`,
                                text: data.message,
                              })
                          }else{
                              Swal.fire({
                                icon: 'error',
                                title: 'Oops...',
                                text: data.message,
                              })
                          }
                      });
        }else{
          Swal.showValidationMessage("Some Fields are empty.");
        }
      }
    })
  }

  return (
  	<>
      <Row>
        <Col>
          <div className='py-5 my-5 container rounded shadow border'>
              <div className="form-group">
                <Container>
                  <Row>
                    <Col>
                      <label htmlFor="courseName">Search Product:</label>
                      <input
                        className='p-3'
                        type="text"
                        id="courseName"
                        className="form-control"
                        value={searchQuery}
                        onChange={event => setSearchQuery(event.target.value)}
                      />
                    </Col>
                  </Row>
                  <Row>
                    <Col className='text-end'>
                      <CreateProduct />
                    </Col>
                  </Row>
                </Container>
              </div>
          </div>
        </Col>
      </Row>
	    <Row className=''>
          <Col className='mb-2'>     
            <Table striped bordered hover responsive className='shadow border'>
              <thead>
                  <tr className="text-center">
                      <th>Name</th>
                      <th>Price</th>
                      <th>Current Stock</th>
                  </tr>
              </thead>
              <tbody>
                {
                  loading ? 
                    <tr>
                      <td className='align-middle text-center' colSpan={3}>
                        <div className='text-center'>
                          <BeatLoader loading={loading} size={12} aria-label="Loading Spinner" data-testid="loader" />  
                        </div>
                      </td>
                    </tr>
                  :
                  products
                }
              </tbody>
          </Table>
          </Col>
	    </Row> 
  	</>
  );
};

