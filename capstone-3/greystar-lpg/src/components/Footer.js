export default function Footer(){
	return (
		<div className='footer bg-dark p-1'>
			<div className='m-3'>
				<a href="https://www.facebook.com/GreystarLPG" class="fa fa-facebook bg-white p-2 me-2 LinkDecor rounded"></a>
				<a href="https://www.instagram.com/greystarlpg/" class="fa fa-instagram bg-white p-2 me-2 LinkDecor rounded"></a>
				<a href="https://www.youtube.com/@greystarlpg9541" class="fa fa-youtube bg-white p-2 LinkDecor rounded"></a>
			</div>
			GREYSTAR LPG BOHOL &copy; 2023
		</div>
	);
}