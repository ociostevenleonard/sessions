import {Button, Modal, Form, Table, Container, Row, Col} from 'react-bootstrap';
import {useState} from 'react';
import Swal from 'sweetalert2';
import {Link} from 'react-router-dom';
import {useParams, useNavigate} from 'react-router-dom';

export default function ResetPassword(){

    const navigate = useNavigate();

	const [userId, setUserId] = useState("");
	const [oldPass, setOldPass] = useState("");
	const [newPass, setNewPass] = useState("");
	const [retypePass, setRetypePass] = useState("");

	const [showEdit, setShowEdit] = useState(false);

	/*const [latestStatus, setLatestStatus] = useState("");
	const [latestUpdate, setLatestUpdate] = useState("");
	const [price, setPrice] = useState("");

	const [orderedItems, setOrderedItems] = useState([]);
	const [actions, setActions] = useState([]);
    const [totalQty, setTotalQty] = useState(0);
    const [totalOrder, setTotalOrder] = useState(0);*/

	const openEdit = () => {
		let QTYholder = 0, TOTHolder = 0;
		
		setShowEdit(true);

		/*fetch(`http://localhost:4002/orders/GetSingleOrder/${orderId}`, {
			method: "GET",
			headers: {
				"Content-Type": "application/json",
				'Authorization': `Bearer ${localStorage.getItem('myToken')}`
			}
		}).then((result) => result.json())
		.then((data) => {
			setOrderId(data._id);
			setLatestStatus(data.actionsTaken[data.actionsTaken.length - 1].action);
			setLatestUpdate(data.actionsTaken[data.actionsTaken.length - 1].action_date);
			setOrderedItems(data.productsPurchased);
			setActions(data.actionsTaken);
			setPrice(data.price);

    		orderedItems.forEach(item => {

				QTYholder += item.quantity;
				TOTHolder += item.subtotal;
    		})

            setTotalQty(QTYholder);
            setTotalOrder(TOTHolder);
		});*/
	};

	const closeEdit = () => {
		setShowEdit(false);
		setOldPass("");
		setNewPass("");
		setRetypePass("");
	};

	const resetPass = (e) =>{
		e.preventDefault();
		fetch(`${process.env.REACT_APP_API_URL}/users/updatePassword/`, {
					method: "PUT",
					headers: {
						"Content-Type": "application/json",
						'Authorization': `Bearer ${localStorage.getItem('myToken')}`
					},
					body: JSON.stringify({
						oldPassword: oldPass,
						newPassword: newPass,
						retypePassword: retypePass
					})
				}).then((result) => result.json())
				.then((data) => {
					if(data.isOk!==true){
						let textPass = data.message;
						if(typeof data.message == "object"){
							textPass = "<ul style='list-style-type: none;'>";
							data.message.forEach(index => {
								textPass += `<li>${index}</li>`
							})
							textPass += "</ul>";
						}
						console.log(typeof data.message);
	                    Swal.fire({
	                      icon: 'error',
	                      title: 'Something went wrong...',
	                      html: textPass
	                    })
	                }else{
	                    Swal.fire({
	                        icon: 'success',
	                        title: 'Success!',
	                      	text: data.message,
	                    });
	                    closeEdit();
	                    /*navigate('/products');*/
	                }
				});
	};

	return (
		<>
			<Link 
			    className='btn btn-secondary me-2'
			    onClick={() => {
			    	openEdit();
			    }}
			>
				Change Password
			</Link>
			<Modal show={showEdit} onHide={() => {
				closeEdit();
			}}>
				<Form onSubmit={(ev) => resetPass(ev)}>
					<Modal.Header closeButton>
						<Modal.Title>Change Password</Modal.Title>
			        </Modal.Header>
			        <Modal.Body>
			        	<Form.Group controlId="courseDescription">
			        	    <Form.Label>Old Password</Form.Label>
			        	    <Form.Control type="password" value={oldPass} required onChange={e => setOldPass(e.target.value)}/>
			        	</Form.Group>
			        	<Form.Group controlId="courseDescription">
			        	    <Form.Label>New Password</Form.Label>
			        	    <Form.Control type="password" value={newPass} required onChange={e => setNewPass(e.target.value)}/>
			        	</Form.Group>
			        	<Form.Group controlId="courseDescription">
			        	    <Form.Label>Retype Password</Form.Label>
			        	    <Form.Control type="password" value={retypePass} required onChange={e => setRetypePass(e.target.value)}/>
			        	</Form.Group>
			        </Modal.Body>
			        <Modal.Footer>
			        	<Button variant="secondary" onClick={()=>{
			        		closeEdit();
			        	}}>Close</Button>
			        	<Button variant="success" type="submit">Submit</Button>
			        </Modal.Footer>
				</Form>
			</Modal>
		</>
	);
}