import Container from 'react-bootstrap/Container';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import {Link, NavLink} from 'react-router-dom';
import {useState, useContext} from 'react';
import React, {Fragment} from 'react';
import UserContext from '../UserContext.js';

export default function AppNavbar(){
	//const [user, setUser] = useState(localStorage.getItem('token'));
	const { user, unsetUser } = useContext(UserContext);
	const [ isAdmin, setIsAdmin ] = useState(false);
	





	
	fetch(`${process.env.REACT_APP_API_URL}/users/viewDetails/`, {
		method: "GET",
		headers: {
			"Content-Type": "application/json",
			'Authorization': `Bearer ${localStorage.getItem('myToken')}`
		}
	}).then((result) => result.json())
	.then((data) => {
		setIsAdmin(data.isAdmin);
	});

	return (
		<Navbar expand="lg" className='sticky-top bg-primary mx-0'>
		    <Container>
				<Navbar.Brand as={NavLink} to="/" className='text-light'>Greystar LPG</Navbar.Brand>
				<Navbar.Toggle aria-controls="basic-navbar-nav" />
				<Navbar.Collapse id="basic-navbar-nav">
					<Nav className="me-auto">
						{/*exact - useful for NavLinks to respond with exact path NOT partial match*/}
						<Nav.Link as={NavLink} to='/' className='text-light' exact>Home</Nav.Link>
						<Nav.Link as={NavLink} to="/products" className='text-light' exact>Products</Nav.Link>
						{
							(user.access !== null)  ? 	
								<React.Fragment>
									{/*FOR ADMINS*/}
									{
										(isAdmin === true) ? 
											<Nav.Link as={NavLink} to="/orders" className='text-light' exact>User Orders</Nav.Link>	
										:
											<Nav.Link as={NavLink} to="/mycart" className='text-light' exact>View Cart</Nav.Link>
									}
									<Nav.Link as={NavLink} to="/profile" className='text-light' exact>My Account</Nav.Link>							
									<Nav.Link as={NavLink} to="/logout" className='text-light' exact>Logout</Nav.Link>
								</React.Fragment>
							:
								//Fragments are use for multiple conditional rendering
								<React.Fragment>
									<Nav.Link as={NavLink} to="/login" className='text-light' exact>Login</Nav.Link>
									<Nav.Link as={NavLink} to="/register" className='text-light' exact>Register</Nav.Link>	
								</React.Fragment>			
						}											
					</Nav>
				</Navbar.Collapse>
		    </Container>
		</Navbar>
	);
}