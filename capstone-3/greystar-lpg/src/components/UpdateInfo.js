import {Button, Modal, Form, Table, Container, Row, Col} from 'react-bootstrap';
import {useState, useEffect, useRef} from 'react';
import Swal from 'sweetalert2';
import {Link} from 'react-router-dom';
import {useParams, useNavigate} from 'react-router-dom';

export default function UpdateInfo(){

    const navigate = useNavigate();

	const [userId, setUserId] = useState("");

	const [fName, setFName] = useState("");
	const [LName, setLName] = useState("");
	const [email, setEmail] = useState("");
	const [contact, setContact] = useState("");
	const [address, setAddress] = useState("");

	const [showEdit, setShowEdit] = useState(false);
	const[isActive, setIsActive] = useState(false);

	const form = useRef();
	/*const [latestStatus, setLatestStatus] = useState("");
	const [latestUpdate, setLatestUpdate] = useState("");
	const [price, setPrice] = useState("");

	const [orderedItems, setOrderedItems] = useState([]);
	const [actions, setActions] = useState([]);
    const [totalQty, setTotalQty] = useState(0);
    const [totalOrder, setTotalOrder] = useState(0);*/

	const openEdit = () => {
		let QTYholder = 0, TOTHolder = 0;
		
		setShowEdit(true);

		fetch(`${process.env.REACT_APP_API_URL}/users/viewDetails/`, {
					method: "GET",
					headers: {
						"Content-Type": "application/json",
						'Authorization': `Bearer ${localStorage.getItem('myToken')}`
					}
				}).then((result) => result.json())
				.then((data) => {
					setFName(data.firstName);
					setLName(data.lastName);
					setEmail(data.email);
					setAddress(data.address);
					setContact(data.contact);
				});
	};

	const closeEdit = () => {
		setShowEdit(false);
		setFName("");
		setLName("");
		setEmail("");
		setAddress("");
		setContact("");
	};

	const updateInfo = (e) =>{
		e.preventDefault();
		fetch(`${process.env.REACT_APP_API_URL}/users/updateUser/`, {
					method: "PUT",
					headers: {
						"Content-Type": "application/json",
						'Authorization': `Bearer ${localStorage.getItem('myToken')}`
					},
					body: JSON.stringify({
						firstName: fName,
						lastName: LName,
						contact: contact,
						address: address
					})
				}).then((result) => result.json())
				.then((data) => {
					if(!data.isOk){
						Swal.fire({
		                    icon: 'error',
		                    title: 'Something went wrong...',
		                    text: data.message,
	                    })
	                }else{
	                    Swal.fire({
	                        icon: 'success',
	                        title: 'Success!',
	                      	text: data.message,
	                    }).then(result => {
	                        window.location.reload();
	                    });
	                    closeEdit();
	                }
				});
	};

	useEffect(() => {
		if((fName !== "" && LName !==""  && email !=="" && address !==""  && contact !=="")){
			setIsActive(true);
		}else{
			setIsActive(false);
		}
	},[fName, LName, email, address, contact]);

	return (
		<>
			<Link 
			    className='btn btn-secondary me-2'
			    onClick={() => {
			    	openEdit();
			    }}
			>
				Update User Info
			</Link>
			<Modal show={showEdit} onHide={() => {
				closeEdit();
			}}>
			<Form onSubmit={(ev) => updateInfo(ev)}>
						<Modal.Header closeButton>
							<Modal.Title>Update User Info</Modal.Title>
				        </Modal.Header>
				        <Modal.Body>
				        	<Form.Group>
				        		<Form.Label>First Name:</Form.Label>
				        		<Form.Control 
				        			name='to_name'
				        			type="text" 
				        			placeholder="Enter First Name"
				        			required value={fName}
				        			onChange={e => {
				        				setFName(e.target.value);
				        			}}
				        		/>
				        	</Form.Group>
				        	<Form.Group>
				        		<Form.Label>Last Name:</Form.Label>
				        		<Form.Control 
				        			type="text" 
				        			placeholder="Enter Last Name" 
				        			required 
				        			value={LName} 
				        			onChange={e => {
				        				setLName(e.target.value);
				        			}}
				        		/>
				        	</Form.Group>
				        	<Form.Group>
				        		<Form.Label>Address:</Form.Label>
				        		<Form.Control 
				        			type="text" 
				        			placeholder="Enter Address" 
				        			required 
				        			value={address} 
				        			onChange={e => {
				        				setAddress(e.target.value);
				        			}}
				        		/>
				        	</Form.Group>
				        	<Form.Group>
				        		<Form.Label>Contact #:</Form.Label>
				        		<Form.Control 
				        			type="number" 
				        			placeholder="Enter Contact #" 
				        			required 
				        			value={contact} 
				        			onChange={e => {
				        				setContact(e.target.value);
				        			}}
				        		/>
				        	</Form.Group>
				        </Modal.Body>
				        <Modal.Footer>
				        	<Button variant="secondary" onClick={()=>{
				        		closeEdit();
				        	}}>Close</Button>
				        	{
				        		isActive ? 
				        		<Button 
				        			variant="success" 
				        			type="submit" 
				        			className='mb-2' 
				        		>Submit</Button> 
				        		:
				        		<Button 
				        			variant="success" 
				        			type="submit" 
				        			className='mb-2'
				        			disabled 
				        		>Submit</Button>  
				        	}
				        </Modal.Footer>




					
					
				</Form>
			</Modal>
		</>
	);
}