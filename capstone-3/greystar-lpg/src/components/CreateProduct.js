import {Button, Modal, Form, Table, Container, Row, Col} from 'react-bootstrap';
import {useState, useEffect, useRef} from 'react';
import Swal from 'sweetalert2';
import {Link} from 'react-router-dom';
import {useParams, useNavigate} from 'react-router-dom';
import axios from 'axios';

export default function CreateProduct(){

    const navigate = useNavigate();

	const [userId, setUserId] = useState("");

	const [name, setName] = useState("");
	const [desc, setDesc] = useState("");
	const [unit, setUnit] = useState("");
	const [price, setPrice] = useState(0);
	const [image, setImage] = useState("");
	const [HTMLImage, setHTMLImage] = useState(require('../images/product-images/default-image.png'));

	const [showEdit, setShowEdit] = useState(false);
	const[isActive, setIsActive] = useState(false);

	const cloudinary = useRef();
	const widget = useRef();

	const openEdit = () => {
		let QTYholder = 0, TOTHolder = 0;
		
		setShowEdit(true);
	};

	const closeEdit = () => {
		setShowEdit(false);
		setName("");
		setDesc("");
		setUnit("");
		setImage("");
		setHTMLImage(require('../images/product-images/default-image.png'));
		setPrice(0);
	};

	const updateInfo = (e) =>{
		e.preventDefault();
		const body = JSON.stringify({
						name: name,
						description: desc,
						unit: unit,
						price: price,
						image: image
					});
		fetch(`${process.env.REACT_APP_API_URL}/products/newProduct/`, {
					method: "POST",
					headers: {
						"Content-Type": "application/json",
						'Authorization': `Bearer ${localStorage.getItem('myToken')}`
					},
					body: body
				}).then((result) => result.json())
				.then((data) => {
					if(!data.isOk){
						Swal.fire({
		                    icon: 'error',
		                    title: 'Something went wrong...',
		                    text: data.message,
	                    })
	                }else{
						Swal.fire({
		                    icon: 'success',
		                    title: 'Sucess',
		                    text: 	data.message
	                    }).then(() => {
	                    	closeEdit();
						    window.location.reload();
						})
	                }
				});


	};

	useEffect(() => {
		if((name !== "" && desc !==""  && unit !=="" && price !=="")){
			setIsActive(true);
		}else{
			setIsActive(false);
		}
	},[name, desc, unit, price]);

	return (
		<>
			<Link 
			    className='btn btn-success mt-3'
			    onClick={() => {
			    	openEdit();
			    }}
			>
				Create New Product
			</Link>
			<Modal show={showEdit} onHide={() => {
				closeEdit();
			}}>
			<Form onSubmit={(ev) => updateInfo(ev)}>
						<Modal.Header closeButton>
							<Modal.Title>Create New Product</Modal.Title>
				        </Modal.Header>
				        <Modal.Body>
				        	<div className='text-center'>
				        		<img alt="Product Image" className='img-size-standard rounded border' src={HTMLImage}/>
				        	</div>
				        	<Form.Group>
				        		<Form.Label>Product Image:</Form.Label>
				        		<Form.Control 
				        			type="file" 
				        			accept="image/png, image/gif, image/jpeg"
				        			placeholder="Upload Image"
				        			required 
				        			onChange={e => {
				        				//setImage(e.target.files[0]);
				        				var file = e.target.files[0]
			        				    let reader = new FileReader()
			        				    reader.readAsDataURL(file)
			        				    reader.onloadend = () => {
			        				      	setImage(reader.result);
			        				      	setHTMLImage(URL.createObjectURL(file));
			        				    };
				        			}}
				        		/>
				        	</Form.Group>
				        	<Form.Group>
				        		<Form.Label>Product Name:</Form.Label>
				        		<Form.Control 
				        			type="text" 
				        			placeholder="Enter Product Name"
				        			required value={name}
				        			onChange={e => {
				        				setName(e.target.value);
				        			}}
				        		/>
				        	</Form.Group>
				        	<Form.Group>
				        		<Form.Label>Description:</Form.Label>
				        		<Form.Control 
				        			type="text" 
				        			placeholder="Enter Description" 
				        			required 
				        			value={desc} 
				        			onChange={e => {
				        				setDesc(e.target.value);
				        			}}
				        		/>
				        	</Form.Group>
				        	<Form.Group>
				        		<Form.Label>Unit:</Form.Label>
				        		<Form.Control 
				        			type="text" 
				        			placeholder="Enter Unit" 
				        			required 
				        			value={unit} 
				        			onChange={e => {
				        				setUnit(e.target.value);
				        			}}
				        		/>
				        	</Form.Group>
				        	<Form.Group>
				        		<Form.Label>Price:</Form.Label>
				        		<Form.Control 
				        			type="number" 
				        			placeholder="Enter Price" 
				        			required 
				        			value={price} 
				        			onChange={e => {
				        				setPrice(e.target.value);
				        			}}
				        		/>
				        	</Form.Group>
				        </Modal.Body>
				        <Modal.Footer>
				        	<Button variant="secondary" onClick={()=>{
				        		closeEdit();
				        	}}>Close</Button>
				        	{
				        		isActive ? 
				        		<Button 
				        			variant="success" 
				        			type="submit" 
				        			className='mb-2' 
				        		>Submit</Button> 
				        		:
				        		<Button 
				        			variant="success" 
				        			type="submit" 
				        			className='mb-2'
				        			disabled 
				        		>Submit</Button>  
				        	}
				        </Modal.Footer>




					
					
				</Form>
			</Modal>
		</>
	);
}