import {Button, Modal, Form, Table, Container, Row, Col} from 'react-bootstrap';
import {useState, useEffect, useRef} from 'react';
import Swal from 'sweetalert2';
import {Link} from 'react-router-dom';
import {useParams, useNavigate} from 'react-router-dom';
import axios from 'axios';

export default function UpdateProductImage({productId}){

    const navigate = useNavigate();

	const [pId, setProductId] = useState("");

	const [imagePublicId, setImagePublicId] = useState("");
	const [imageAddress, setImageAddress] = useState("");
	const [newImage, setNewImage] = useState(require('../images/product-images/default-image.png'))

	const [showEdit, setShowEdit] = useState(false);
	const[isActive, setIsActive] = useState(true);

	const cloudinary = useRef();
	const widget = useRef();

	const openEdit = () => {
		setShowEdit(true);

		fetch(`${process.env.REACT_APP_API_URL}/products/getproduct/${productId}`, {
		        method: "GET",
		        headers: {
		            "Content-Type": "application/json"
		        }
		    }).then((result) => result.json())
		    .then((data) => {
		        setImageAddress(data.image.url);
		        setImagePublicId(data.image.public_id);
		    });

	};

	const closeEdit = () => {
		setShowEdit(false);
		setNewImage(require('../images/product-images/default-image.png'));
	};

	const updateInfo = (e) =>{
		e.preventDefault();
		const body = JSON.stringify({
						old_image_link: imagePublicId,
						image: newImage
					});
		fetch(`${process.env.REACT_APP_API_URL}/products/UpdateProductImage/${productId}`, {
					method: "PUT",
					headers: {
						"Content-Type": "application/json",
						'Authorization': `Bearer ${localStorage.getItem('myToken')}`
					},
					body: body
				}).then((result) => result.json())
				.then((data) => {

					
					if(!data.isOk){
						Swal.fire({
		                    icon: 'error',
		                    title: 'Something went wrong...',
		                    text: data.message,
	                    })
	                }else{
	                    Swal.fire({
		                    icon: 'success',
		                    title: 'Sucess',
		                    text: 	data.message
	                    }).then(() => {
	                    	closeEdit();
						    window.location.reload();
						})
	                }
				}).catch(e => console.log(e));

	};

	return (
		<>
			<Link 
			    className='btn btn-secondary w-100'
			    onClick={() => {
			    	openEdit();
			    }}
			>
				Update Image
			</Link>
			<Modal show={showEdit} onHide={() => {
				closeEdit();
			}}>
			<Form onSubmit={(ev) => updateInfo(ev)}>
						<Modal.Header closeButton>
							<Modal.Title>Update Product Image</Modal.Title>
				        </Modal.Header>
				        <Modal.Body>
				        	<div className='text-center'>
				        		<img alt="Product Image" className='img-size-standard rounded border' src={imageAddress}/>
				        	</div>
				        	<Form.Group>
				        		<Form.Label>New Product Image:</Form.Label>
				        		<Form.Control 
				        			type="file" 
				        			accept="image/png, image/gif, image/jpeg"
				        			placeholder="Upload Image"
				        			required 
				        			onChange={e => {
				        				//setImage(e.target.files[0]);
				        				var file = e.target.files[0]
			        				    let reader = new FileReader()
			        				    reader.readAsDataURL(file)
			        				    reader.onloadend = () => {
			        				      	setNewImage(reader.result);
			        				      	setImageAddress(URL.createObjectURL(file));
			        				    };
				        			}}
				        		/>
				        	</Form.Group>
				        </Modal.Body>
				        <Modal.Footer>
				        	<Button variant="secondary" onClick={()=>{
				        		closeEdit();
				        	}}>Close</Button>
				        	{
				        		isActive ? 
				        		<Button 
				        			variant="success" 
				        			type="submit" 
				        			className='mb-2' 
				        		>Submit</Button> 
				        		:
				        		<Button 
				        			variant="success" 
				        			type="submit" 
				        			className='mb-2'
				        			disabled 
				        		>Submit</Button>  
				        	}
				        </Modal.Footer>
				</Form>
			</Modal>
		</>
	);
}