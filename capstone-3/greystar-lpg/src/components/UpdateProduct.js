import {Button, Modal, Form, Table, Container, Row, Col} from 'react-bootstrap';
import {useState, useEffect, useRef} from 'react';
import Swal from 'sweetalert2';
import {Link} from 'react-router-dom';
import {useParams, useNavigate} from 'react-router-dom';
import axios from 'axios';

export default function UpdateProduct({productId}){

    const navigate = useNavigate();

	const [pId, setProductId] = useState("");

	const [name, setName] = useState("");
	const [desc, setDesc] = useState("");
	const [unit, setUnit] = useState("");
	const [price, setPrice] = useState(0);

	const [showEdit, setShowEdit] = useState(false);
	const[isActive, setIsActive] = useState(false);

	const cloudinary = useRef();
	const widget = useRef();

	const openEdit = () => {
		setShowEdit(true);

		fetch(`${process.env.REACT_APP_API_URL}/products/getproduct/${productId}`, {
		        method: "GET",
		        headers: {
		            "Content-Type": "application/json"
		        }
		    }).then((result) => result.json())
		    .then((data) => {
		        setName(data.name);
		        setDesc(data.description);
		        setPrice(data.price);
		        setUnit(data.unit);
		    });

	};

	const closeEdit = () => {
		setShowEdit(false);
		setName("");
		setDesc("");
		setUnit("");
		setPrice(0);
	};

	const updateInfo = (e) =>{
		e.preventDefault();
		const body = JSON.stringify({
						name: name,
						description: desc,
						unit: unit,
						price: price
					});
		fetch(`${process.env.REACT_APP_API_URL}/products/updateInfo/${productId}`, {
					method: "PUT",
					headers: {
						"Content-Type": "application/json",
						'Authorization': `Bearer ${localStorage.getItem('myToken')}`
					},
					body: body
				}).then((result) => result.json())
				.then((data) => {
					if(!data.isOk){
						Swal.fire({
		                    icon: 'error',
		                    title: 'Something went wrong...',
		                    text: data.message,
	                    })
	                }else{
	               		Swal.fire({
		                    icon: 'success',
		                    title: 'Success',
		                    text: 	data.message
	                    }).then(() => {
	                    	closeEdit();
						    window.location.reload();
						})
	                }
				});


	};

	useEffect(() => {
		if((name !== "" && desc !==""  && unit !=="" && price !=="")){
			setIsActive(true);
		}else{
			setIsActive(false);
		}
	},[name, desc, unit, price]);

	return (
		<>
			<Link 
			    className='btn btn-secondary w-100'
			    onClick={() => {
			    	openEdit();
			    }}
			>
				Update Info
			</Link>
			<Modal show={showEdit} onHide={() => {
				closeEdit();
			}}>
			<Form onSubmit={(ev) => updateInfo(ev)}>
						<Modal.Header closeButton>
							<Modal.Title>Update Product Info</Modal.Title>
				        </Modal.Header>
				        <Modal.Body>
				        	<Form.Group>
				        		<Form.Label>Product Name:</Form.Label>
				        		<Form.Control 
				        			type="text" 
				        			placeholder="Enter Product Name"
				        			required value={name}
				        			onChange={e => {
				        				setName(e.target.value);
				        			}}
				        		/>
				        	</Form.Group>
				        	<Form.Group>
				        		<Form.Label>Description:</Form.Label>
				        		<Form.Control 
				        			type="text" 
				        			placeholder="Enter Description" 
				        			required 
				        			value={desc} 
				        			onChange={e => {
				        				setDesc(e.target.value);
				        			}}
				        		/>
				        	</Form.Group>
				        	<Form.Group>
				        		<Form.Label>Unit:</Form.Label>
				        		<Form.Control 
				        			type="text" 
				        			placeholder="Enter Unit" 
				        			required 
				        			value={unit} 
				        			onChange={e => {
				        				setUnit(e.target.value);
				        			}}
				        		/>
				        	</Form.Group>
				        	<Form.Group>
				        		<Form.Label>Price:</Form.Label>
				        		<Form.Control 
				        			type="number" 
				        			placeholder="Enter Price" 
				        			required 
				        			value={price} 
				        			onChange={e => {
				        				setPrice(e.target.value);
				        			}}
				        		/>
				        	</Form.Group>
				        </Modal.Body>
				        <Modal.Footer>
				        	<Button variant="secondary" onClick={()=>{
				        		closeEdit();
				        	}}>Close</Button>
				        	{
				        		isActive ? 
				        		<Button 
				        			variant="success" 
				        			type="submit" 
				        			className='mb-2' 
				        		>Submit</Button> 
				        		:
				        		<Button 
				        			variant="success" 
				        			type="submit" 
				        			className='mb-2'
				        			disabled 
				        		>Submit</Button>  
				        	}
				        </Modal.Footer>




					
					
				</Form>
			</Modal>
		</>
	);
}