import {Button, Row, Col, Image, Container, Table} from 'react-bootstrap';
import {Link, NavLink} from 'react-router-dom';

export default function Banner({passed_data}){
	const {title, content, destination, label} = passed_data;
	return (
		<>
			<div className='bg-greystar'>
					<Image src={require('../images/system/banner.jpg')} className='w-100'/>
					<div className='p-5'>
						<Container className='rounded text-white'>
							<h3 className='py-3 text-primary'>ABOUT US</h3>
							<p className='justity-p'><strong>FIRST GNB ENERGY CORPORATION</strong> is a Filipino owned and managed company with
							major focus in fuel, lubricant and LPG. The company is the exclusive partner of Total
							Philippines Corporation in the Province of Bohol. The leadership of the company is
							committed, responsive and reliable and will be proud to have you as our customer.
							Whatever environment your operations are, we are flexible to do our best to service you
							at the top of our ability because we know what you want to power your business.
							</p>
							<p className='justity-p'>
								Established in 2013 with corporation address at Sampaguita Village, Brgy. Poblacion
								Bien Unido, Bohol. Presently, our main office is located at 2nd floor QVC Building, CPG
								North Avenue Poblacion II, Tagbilaran City.
							</p>
							<p className='justity-p'>
								A leading player in Fuel, Lubricant and LPG market in the Province of Bohol supplying
								more than 100 retail gas stations and 17 LPG outlets and more to come.
							</p>
							<p className='justity-p'>
								We have 2 million-liter capacity Fuel Depot and 20 Tons Capacity LPG refilling Plant
								both are located in the town of Bien Unido, Bohol.
							</p>
						</Container>
					</div>
					<div className=''>
						<Container className='rounded py-3 forHome'>
							<Row>
								<Col className='offset-lg-2 col-lg-4 col-sm-10'>
									<div className='text-center'><h3 className='text-primary'>OUR VISION</h3></div>
									<p className='justity-p'>A cleaner, more energy-efficient, sustainable environment to benefits the world’s present and future generations.</p>									
								</Col>
								<Col className='col-lg-4  col-sm-10'>
									<div className='text-center'><h3 className='text-primary'>OUR MISSION</h3></div>
									<p className='justity-p'>To provide quality superior service and continuously increase our market share in fuel, lubricant and LPG products.</p>									
								</Col>
							</Row>
						</Container>
					</div>
					<div>
						<Container className='my-5 text-white'>
							<h3 className='pt-3 text-primary'>WHY CHOOSE US</h3>
							<h4 className='pb-3 text-primary'><em>FIRST GNB ENERGY CORPORATION</em></h4>
							<p className='justity-p'>
								We are fully committed to the sustainable growth of your businesses and the communities you
								support. Our objective is to be efficient without compromising on safety and to deliver on our
								promise of premium quality products and service that will make a beneficial and lasting
								difference.
							</p>
							<p className='justity-p'>
								We believe wholeheartedly in, and are committed to each other, our value system and the work that we do so we can make difference with consistent quality, excellence and service.
							</p>
						</Container>
					</div>
					<div className='px-3 py-3 mt-3'>
						<Container className='forHome rounded py-3 px-5'>
							<Row>
								<Col className='col-lg-4 col-md-6 col-sm-12 '>
									<h5 className='text-primary'>CORE VALUES</h5>
									<ul>
										<li>Honesty</li>
										<li>Integrity</li>
										<li>Compliance</li>
										<li>Excellence</li>
										<li>Customer satisfaction</li>
									</ul>
								</Col>
								<Col className='col-lg-4 col-md-6 col-xs-12'>
									<h5 className='text-primary'>STRATEGIES</h5>
									<ul>
										<li>Quality Services</li>
										<li>Through Extensive Strategy</li>
										<li>Partnerships</li>
										<li>Environmental Friendly</li>
										<li>Socially Responsible</li>
									</ul>
								</Col>
								<Col className='col-lg-4 col-md-6 col-sm-12 text-center'>
									<h5 className='text-primary'>OUR OBJECTIVES</h5>
									<ul>
										<p className='justity-p'>
											To be a close-knit business
											creating value for its
											shareholders, clients and
											employees through growth
											and optimization.
										</p>
									</ul>
								</Col>
							</Row>
						</Container>
					</div>
					asdasd
			</div>
		</>
	);
};


