import {Card, Button, Row, Col, Container} from 'react-bootstrap';
import PropTypes from 'prop-types';
import {useState} from 'react';
import {Link} from 'react-router-dom'
import UserContext from "../UserContext.js";

/*import coursesDatabase from '../data/coursesDatabase.js';*/

export default function ProductCards({productProp}){
	const {_id, name, description, price, unit, availableStock, image} = productProp;

	/*
	Use the state hook in this component to be able to store its state
		Syntax
			const [getter, setter] = useState(initialGetterValue);

	*/
	const [count, setCount] = useState(0);
	const [seatCount, setSeatCount] = useState(30);

	/*function enroll(){
		if(seatCount > 0){
			setCount(count + 1);
			setSeatCount(seatCount - 1);
		}else{
			alert("No more seats available.");
		}
	}*/

	return (
		<Col className='mb-2'>
	        <Link className='LinkDecor' to={`/products/${_id}`}>
				<Card className='shadow h-100'>
				      <Card.Img variant="top" src={image.url} />
				      <Card.Body className='text-center'>
				        <Card.Title>{name}</Card.Title>
				        <Card.Subtitle className='py-3'>
				        	&#8369; {price} / {unit}
				        </Card.Subtitle>
				        	
				      </Card.Body>
				</Card>
			</Link>
		</Col>
	);
}


// Checks if the CourseCard component is getting the correct prop types.
ProductCards.propTypes = {
	course: PropTypes.shape({
		name: PropTypes.string.isRequired,
		description: PropTypes.string.isRequired,
		price: PropTypes.number.isRequired
	})
};