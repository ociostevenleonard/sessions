import React, { useState, useEffect } from 'react';
import {Col, Row, Container} from 'react-bootstrap';
import ProductCards from './ProductCards.js';
import Swal from 'sweetalert2';
import BeatLoader from "react-spinners/BeatLoader";

export default function SearchBar ({productData}){
  const [searchQuery, setSearchQuery] = useState('');
  const [searchResults, setSearchResults] = useState([]);

  const [products, setProducts] = useState([])
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    handleSearch(searchQuery, true);
  }, [searchQuery])

  const handleSearch = async (data_input) => {
    setLoading(true);
    let body = { 
        searchBar: data_input,
        isActive: true
    }
    fetch(`${process.env.REACT_APP_API_URL}/products/SearchProductByName/`, {
        method: "POST",
        headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify(body)
      }).then((result) => result.json())
    	.then((data) => {
        setLoading(false);
    		if(data.isOk === false){
    			Swal.fire({
    			  icon: 'error',
    			  title: 'Oops...',
    			  text: data.message,
    			})
    		}else{
	    		const ProductArr = data.map(product => {
	    		    if(product.isActive === true) {
	    		        return (
	    		            <ProductCards productProp={product} key={product._id}/>
	    		            )
	    		    } else {
	    		        return null;
	    		    }
	    		})

	    		//set the courses state to the result of our map function, to bring our returned course component outside of the scope of our useEffect where our return statement below can see.
	    		setProducts(ProductArr)
    		}
    	});
  };

  return (
  	<>
	    <div className='py-5 my-5 container rounded shadow border'>
	      	<div className="form-group">
	      		<Container>
	      			<Row>
	      				<Col>
				        	<label htmlFor="courseName">Search Product:</label>
				        	<input
				        		className='p-3'
						        type="text"
						        id="courseName"
						        className="form-control"
						        value={searchQuery}
						        onChange={event => setSearchQuery(event.target.value)}
				        	/>
	      				</Col>
	      			</Row>
	      		</Container>
	      	</div>
	    </div>
      <Row className='p-1'>
        <Col className='col-12 text-center '>
          <BeatLoader loading={loading} size={12} aria-label="Loading Spinner" data-testid="loader" />  
        </Col>
      </Row> 
	    <Row className='text-center ' xs={1} md={3} lg={6}>
	        { products }
	    </Row> 
  	</>
  );
};

