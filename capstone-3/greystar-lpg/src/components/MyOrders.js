import {Row, Container, Button} from 'react-bootstrap';
import React, { useState, useEffect } from 'react';
import ViewOrder from './ViewOrder.js';
import {Link} from 'react-router-dom';

export default function MyOrders({data_from_user_orders}) {
    const {_id, actionsTaken,productsPurchased } = data_from_user_orders;

    let text_color = "text-success";
    if(actionsTaken[actionsTaken.length - 1].action === 'Pending'){
        text_color = 'text-info';
    }else if(actionsTaken[actionsTaken.length - 1].action === 'On Process'){
        text_color = 'text-warning';
    }
    

    return(  
        <tr key={_id}>
            <td className='align-middle text-center'>
                <ViewOrder order={_id} />
            </td>
            <td className={`${text_color} font-weight-bold align-middle text-center`}>
                {actionsTaken[actionsTaken.length - 1].action}
            </td>
            <td className='align-middle text-center'>
                {actionsTaken[actionsTaken.length - 1].action_date}
            </td>
            <td className='align-middle'>
                {actionsTaken[actionsTaken.length - 1].remarks}
            </td>  
        </tr>
    );
}