import React, { useState, useEffect } from 'react';
import {Col, Row, Container, Table} from 'react-bootstrap';
import Swal from 'sweetalert2';
import {Link} from 'react-router-dom';

export default function ProductInfo_Details ({product_info}){
	const {_id, name, description, price, unit, availableStock, image, isActive} = product_info;
	const priceString = (0 + price).toFixed(2);
	let ProductStatus = (isActive) ? {
		status: "Active",
		color: "text-success"
	} : {
		status: "Archived",
		color: "text-danger"
	};

  return (
  	<>
      <div className='mt-3 p-3 border rounded shadow'>
		<Table>
		    <tbody>
		    	<tr>
		    		<td colSpan={2} className='text-center'>
		    		<h5>PRODUCT DETAILS</h5>
		    		<hr />
		    			<img alt="Product Image" className='img-size-standard rounded border mb-3' src={image}/>

		    		</td>
		    	</tr>
		    	<tr>
		    		<td>
		    			PRODUCT NAME:
		    		</td>
		    		<td>
		    			<h5>{name}</h5>
		    		</td>
		    	</tr>
		    	<tr>
		    		<td>
		    			PRODUCT DESCRIPTION:
		    		</td>
		    		<td>
		    			<h5>{description}</h5>
		    		</td>
		    	</tr>
		    	<tr>
		    		<td>
		    			UNIT:
		    		</td>
		    		<td>
		    			<h5>{unit}</h5>
		    		</td>
		    	</tr>
		    	<tr>
		    		<td>
		    			PRICE:
		    		</td>
		    		<td>
		    			<h5>&#8369; {priceString}</h5>
		    		</td>
		    	</tr>
		    	<tr>
		    		<td>
		    			AVAILABLE STOCK:
		    		</td>
		    		<td>
		    			<h5>{availableStock}</h5>
		    		</td>
		    	</tr>
		    	<tr>
		    		<td>
		    			STATUS:
		    		</td>
		    		<td>
		    			<h5 className={ProductStatus.color}>{ProductStatus.status}</h5>
		    		</td>
		    	</tr>
		    </tbody>
		</Table>
      </div>
  	</>
  );
};

