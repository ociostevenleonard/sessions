import {Row, Container, Col, Card, Table} from 'react-bootstrap';
import React, { useState, useEffect, useContext } from 'react';
import UserContext from "../UserContext.js";
import {useParams, useNavigate, Navigate} from 'react-router-dom';

import ProductInfo_Details from '../components/ProductInfo_Details.js';
import UpdateProduct from '../components/UpdateProduct.js';
import UpdateProductImage from '../components/UpdateProductImage.js';
import ProductStockInfo from '../components/ProductStockInfo.js';
import Product_Stock_Deliveries from '../components/Product_Stock_Deliveries.js';

import Swal from 'sweetalert2';
import {Link} from 'react-router-dom';

import BeatLoader from "react-spinners/BeatLoader";

export default function ProductInfo() {
	const navigate = useNavigate();
    let { prodID } = useParams();

	const [name, setName] = useState("");
	const [desc, setDesc] = useState("");
	const [unit, setUnit] = useState("");
	const [price, setPrice] = useState(0);
	const [isAdmin, setIsAdmin] = useState(false);
	const [prodData, setProductData] = useState("");

	const [id, setId] = useState(prodID);

	const [g_dels, setDels] = useState(0);
	const [rets, setRets] = useState(0);
	const [solds, setSolds] = useState(0);


	const [delData, setDelData] = useState([]);
	const [retData, setRetData] = useState([]);
	const [soldData, setSoldData] = useState([]);

  	const [loading, setLoading] = useState(false);
	


	function getTotals(ProductID){
		fetch(`${process.env.REACT_APP_API_URL}/products/showDeliveries/${prodID}`, {
			method: "GET",
			headers: {
				"Content-Type": "application/json",
				'Authorization': `Bearer ${localStorage.getItem('myToken')}`
			}
		}).then((result) => result.json())
		.then((data) => {
			let holder = 0;
			data.forEach(item => {
				holder += (item.quantity);
			})
			setDels(holder);
			setDelData(data);
		});

		fetch(`${process.env.REACT_APP_API_URL}/products/showTrash/${prodID}`, {
					method: "GET",
					headers: {
						"Content-Type": "application/json",
						'Authorization': `Bearer ${localStorage.getItem('myToken')}`
					}
				}).then((result) => result.json())
				.then((data) => {
					let holder = 0;
					data.forEach(item => {
						holder += (item.quantity);
					})
					setRets(holder);
					setRetData(data);
				});

		fetch(`${process.env.REACT_APP_API_URL}/products/ShowSoldTotal/${prodID}`, {
					method: "GET",
					headers: {
						"Content-Type": "application/json",
						'Authorization': `Bearer ${localStorage.getItem('myToken')}`
					}
				}).then((result) => result.json())
				.then((data) => {
					setSolds(data.total);
					setSoldData(data.data);
					console.log(data.data)
				});
	}

	function refresh(){
		setLoading(true);
	  	fetch(`${process.env.REACT_APP_API_URL}/products/getproduct/${prodID}`, {
    		method: "GET",
    		headers: {
				"Content-Type": "application/json"
			}
    	}).then((result) => result.json())
    	.then((data) => {
    		setLoading(false);
    		data.image = data.image.url
			setProductData(data);
		  	getTotals(prodID);
    	});
	}

	function toArchive(ProductId, ProductName, action){
	      Swal.fire({
	        title: `${action} this item?`,
	        text: `${ProductName}`,
	        icon: 'warning',
	        showCancelButton: true,
	        confirmButtonColor: '#3085d6',
	        cancelButtonColor: '#d33',
	        confirmButtonText: `Yes, let's proceed!`
	      }).then((result) => {
	        if (result.isConfirmed) {
	          fetch(`${process.env.REACT_APP_API_URL}/products/${ProductId}/${action}`, {
	              method: "PUT",
	              headers: {
	                  "Content-Type": "application/json",
	                  'Authorization': `Bearer ${localStorage.getItem('myToken')}`
	              },
	              body: JSON.stringify({
	                  prodID: ProductId
	              })
	          }).then((result) => result.json())
	          .then((data) => {
	              if(data.isOk === true){   
	                refresh();
	              }else{
	                  Swal.fire({
	                    icon: 'error',
	                    title: 'Oops...',
	                    text: data.message,
	                  })
	              }
	          });
	        }
	      });
	  }


	function stockManage(ProductId, ProductName, action, input2Placeholder){
	  Swal.fire({
	    title: `${action} to ${ProductName}`,
	    html:
	    '<input id="qty" type="number" placeholder="Quantity" class="swal2-input" required>' +
	    '<input id="delBy" type="text" placeholder="'+ input2Placeholder +'" class="swal2-input" required>',
	    focusConfirm: false,
	    icon: 'warning',
	    showCancelButton: true,
	    preConfirm: () => {
	      const qty = document.getElementById('qty').value;
	      const deliveredBy = document.getElementById('delBy').value
	      let forBody = (action == 'addStock') ? {
	        quantity: qty,
	        deliveredBy: deliveredBy
	      } : {
	        quantity: qty,
	        reasonForTrashing: deliveredBy
	      }
	      if(qty && deliveredBy){
	        fetch(`${process.env.REACT_APP_API_URL}/products/${action}/${ProductId}`, {
	                        method: "POST",
	                        headers: {
	                            "Content-Type": "application/json",
	                            'Authorization': `Bearer ${localStorage.getItem('myToken')}`
	                        },
	                        body: JSON.stringify(forBody)
	                    }).then((result) => result.json())
	                    .then((data) => {
	                        if(data.isOk === true){       
	                			Swal.fire({
	                			  position: 'top-end',
	                			  icon: 'success',
	                			  title: data.message,
	                			  showConfirmButton: false,
	                			  timer: 1500
	                			}).then(result => {
		                        	refresh();
	                			})
	                        }else{
	                            Swal.fire({
	                              icon: 'error',
	                              title: 'Oops...',
	                              text: data.message,
	                            })
	                        }
	                    });
	      }else{
	        Swal.showValidationMessage("Some Fields are empty.");
	      }
	    }
	  })
	}


	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/users/viewDetails/`, {
				method: "GET",
				headers: {
					"Content-Type": "application/json",
					'Authorization': `Bearer ${localStorage.getItem('myToken')}`
				}
			}).then((result) => result.json())
			.then((data) => {
				if(data.isAdmin){
					refresh();
				}else{
					Swal.fire({
					  icon: 'error',
					  title: 'Oops...',
					  text: 'Restricted Access'
					}).then(result=>{
						navigate('/');
					})
				}
			});
	}, [])

	function display(){
		return (
			loading ? 
				<div className='text-center p-5'>
					<BeatLoader loading={loading} size={12} aria-label="Loading Spinner" data-testid="loader" />  
				</div>
			:
				<Container  className='pb-3'>
						<Row>
		                	<Col className='offset-lg-1 col-lg-6 '>
		                		<Container>
		                			 <ProductInfo_Details product_info={prodData} />
		                		</Container>
		                	</Col>
		                	<Col className='col-lg-4'>
		                		<Container className='mt-3 border rounded p-3 shadow'>
		                			<div className='text-center'><h5>ACTIONS</h5></div>
		                			<hr />
		                			<div className='p-2'><UpdateProductImage productId={prodData._id}  /></div>
		                			<div className='p-2'><UpdateProduct productId={prodData._id} /></div>
		                			<div className='p-2'>
		                				{
		                				  (prodData.isActive) ? 
		                				    <Link 
		                				      className='btn btn-danger w-100'
		                				      onClick={(ev) => {
		                				        toArchive(prodData._id, prodData.name, 'archive');
		                				      }}
		                				    >
		                				      Archive Item
		                				    </Link>
		                				  :
		                				    <Link 
		                				        className='btn btn-success w-100'
		                				        onClick={() => {
		                				          toArchive(prodData._id, prodData.name, 'activate');
		                				        }}
		                				    >
		                				      Activate Item
		                				    </Link>
		                				}
		                			</div>
		                			<div className='p-2'>
				            			<Link
				            				className='btn btn-success w-100'
										    onClick={() => {
										      stockManage(prodData._id, prodData.name, 'addStock', 'Delivered By');
										    }}
										>
										  Add Stock
										</Link>
		                			</div>
		                			<div className='p-2'>
		                				<Link 
		                				    className='btn btn-warning w-100'
		                				    onClick={() => {
		                				      stockManage(prodData._id, prodData.name, 'removeStock', 'Reason');
		                				    }}
		                				>
		                				  Remove Stock
		                				</Link>
		                			</div>
		                		</Container>
		                	</Col>
		                </Row>
	                	<ProductStockInfo productProp={{
	                		dels: g_dels,
	                		rets: rets,
	                		sold: solds,
	                		datas: delData,
	                		retdatas: retData,
	                		soldData: soldData
	                	}}/>
		        </Container>
			
		);
	}
    return(
        <>
        	{
        		(localStorage.getItem('myToken') == undefined)  ?
					<Navigate to='/'/>
				:
					display()
        	}
        </>
            )
}