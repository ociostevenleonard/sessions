import {useState, useEffect, useContext} from 'react';
import {Container, Card, Button, Row, Col} from 'react-bootstrap';
import {useParams, useNavigate} from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from "../UserContext.js";
import {Link, NavLink} from 'react-router-dom';

export default function CourseView(){
    /*Curly brace must be used on useParams and useContext*/
    const {user, setUser} = useContext(UserContext);
    const { prodID } = useParams();

    const navigate = useNavigate();

	const [name, setName] = useState("");
    const [course, setCourse] = useState("")
	const [description, setDescription] = useState("");
    const [image, setImage] = useState("");
	const [price, setPrice] = useState(0);
    const [stock, setStock] = useState(0);
    const [quantity, setQuantity] = useState(1);
    const [isEnrolled, setIsEnrolled] = useState(false);




    const addToCart = (productId_parameter) => {
        if(quantity < 1){
            Swal.fire({
              icon: 'error',
              title: 'Something went wrong...',
              text: `Quantity must be greater than 0.`,
            })
        }else if(quantity > stock){
            Swal.fire({
              icon: 'error',
              title: 'Something went wrong...',
              text: `Insufficient stocks`,
            })
        }else{
            fetch(`${process.env.REACT_APP_API_URL}/orders/addToCart/`, {
                method: "POST",
                headers: {
                    "Content-Type": "application/json",
                    'Authorization': `Bearer ${localStorage.getItem('myToken')}`
                },
                body: JSON.stringify({
                    productId: productId_parameter,
                    quantity: quantity
                })
            }).then((result) => result.json())
            .then((data) => {
                if(data.isOk!==true){
                    Swal.fire({
                      icon: 'error',
                      title: 'Something went wrong...',
                      text: data.message,
                    })
                }else{
                    Swal.fire({
                        icon: 'success',
                        title: 'Success!',
                        text: `Added ${name} (Qty: ${quantity}) to Cart.`,
                    });

                    navigate('/products');
                }
            });
        }
    }

    useEffect(()=>{
        fetch(`${process.env.REACT_APP_API_URL}/products/getproduct/${prodID}`, {
                method: "GET",
                headers: {
                    "Content-Type": "application/json"
                }
            }).then((result) => result.json())
            .then((data) => {
                setName(data.name);
                setDescription(data.description);
                setPrice(data.price.toFixed(2));
                setStock(data.availableStock);
                setImage(data.image.url);
                if(user.access !== null){
                    
                }
            });
        }, [prodID]);

	return(
		<Container className="mt-5">
            <Row>
                <Col lg={{ span: 6, offset: 3 }}>
                    <Card>
                        <Card.Body>
                            <Container>
                                <Row>
                                    <Col className='col-6'>
                                        <div className='bg-primary rounded'>
                                            <Card.Img src={image} className='img-fluid' />

                                            <Card.Title className="p-3 text-center bg-primary text-white ">{name}</Card.Title>
                                        </div>
                                        <Card.Text>Available Stock: {stock}</Card.Text>
                                    </Col>
                                    <Col className='col-6'>
                                        <Card.Subtitle>Description:</Card.Subtitle>
                                        <Card.Text className="pb-3">{description}</Card.Text>
                                        <Card.Subtitle>Price:</Card.Subtitle>
                                        <Card.Text className="pb-3">&#8369; {price}</Card.Text>
                                        <Card.Text te>
                                            Quantity: 
                                            <div className='text-center'>
                                                <h4>{quantity}</h4>
                                                <Button 
                                                    className='mx-1'
                                                    variant="danger"
                                                    onClick={() => {
                                                        /*enroll(courseId);*/
                                                        if(quantity > 0){
                                                            setQuantity(quantity-1);
                                                        }
                                                    }}
                                                >-</Button>
                                                <Button 
                                                    className='mx-1 '
                                                    variant="success"
                                                    onClick={() => {
                                                        /*enroll(courseId);*/
                                                        if(quantity < stock){
                                                            setQuantity(quantity+1);
                                                        }
                                                    }}
                                                >+</Button>
                                            </div>

                                        </Card.Text>
                                        <div className='text-center'>
                                            {
                                                (user.access !== null)  ?
                                                    <Button 
                                                        className=''
                                                        variant="primary"
                                                        onClick={() => {
                                                            /*enroll(courseId);*/
                                                            addToCart(prodID)
                                                        }}
                                                    >Add to Cart</Button>
                                                :
                                                    <Button variant='danger'as={NavLink} to={'/login'} exact>
                                                        Please Login First
                                                    </Button>
                                            } 
                                        </div>  
                                    </Col>
                                </Row>
                            </Container>
                        </Card.Body>        
                    </Card>
                </Col>
            </Row>
        </Container>
		)
}