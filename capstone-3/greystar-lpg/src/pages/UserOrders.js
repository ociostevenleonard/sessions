import { useState, useEffect } from 'react';
import { Table, Button, Container, Row, Col } from 'react-bootstrap';
import { useNavigate } from "react-router-dom";

import Swal from 'sweetalert2';

export default function UserOrder() {

    const navigate = useNavigate();

    const [cart, setCart] = useState([]);
    const [quantity, setQuantity] = useState(0);
    const [totalQty, setTotalQty] = useState(0);
    const [totalOrder, setTotalOrder] = useState(0);
    const [proceed, setProceed] = useState(false);
    const [clickCtr, setClickCtr] = useState(0);
    
    const AddQuantity = (CartItem_Id, CurrentQuantity) =>{
        /*let Qty = CurrentQuantity + 1;
        fetch(`${process.env.REACT_APP_API_URL}/orders/changeQuantity/`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                'Authorization': `Bearer ${localStorage.getItem('myToken')}`
            },
            body: JSON.stringify({
                productId: CartItem_Id,
                quantity: Qty
            })
        }).then((result) => result.json())
        .then((data) => {
            if(!data.isOk){
                Swal.fire({
                  icon: 'error',
                  title: 'Something went wrong...',
                  text: data.message
                })
            }else{
                setClickCtr(clickCtr + 1);
                console.log(clickCtr);
            }
        });*/
    }

    const DeductQuantity = (CartItem_Id, CurrentQuantity) =>{
        /*if (CurrentQuantity > 1){
            let Qty = CurrentQuantity - 1;
            fetch(`${process.env.REACT_APP_API_URL}/orders/changeQuantity/`, {
                method: "POST",
                headers: {
                    "Content-Type": "application/json",
                    'Authorization': `Bearer ${localStorage.getItem('myToken')}`
                },
                body: JSON.stringify({
                    productId: CartItem_Id,
                    quantity: CurrentQuantity - 1
                })
            }).then((result) => result.json())
            .then((data) => {
                if(!data.isOk){
                    Swal.fire({
                      icon: 'error',
                      title: 'Something went wrong...',
                      text: data.message
                    })
                }else{
                    setClickCtr(clickCtr + 1);
                    console.log(clickCtr);
                }
            });
        }else{
            Swal.fire({
              icon: 'error',
              title: 'Something went wrong...',
              text: `Quantity must not be 0.`
            })
        }*/
    }

    function toArchive(ProductId){
        /*Swal.fire({
          title: `Are you sure?`,
          text: `Product ID` + ProductId + "...",
          icon: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: `Yes, let's proceed!`
        }).then((result) => {
          if (result.isConfirmed) {
            fetch(`${process.env.REACT_APP_API_URL}/orders/removeItem/`, {
                method: "PUT",
                headers: {
                    "Content-Type": "application/json",
                    'Authorization': `Bearer ${localStorage.getItem('myToken')}`
                },
                body: JSON.stringify({
                    productId: ProductId
                })
            }).then((result) => result.json())
            .then((data) => {
                console.log(data);
                if(data.isOk === true){
                    Swal.fire({
                      icon: 'success',
                      title: `Successfully Removed`,
                      text: data.message,
                    }).then(result => {
                        setClickCtr(clickCtr + 1);
                        console.log(clickCtr);
                    })
                }else{
                    Swal.fire({
                      icon: 'error',
                      title: 'Oops...',
                      text: data.message,
                    })
                }
            });
          }
        });*/
    }

    function CheckOut(){
        /*Swal.fire({
          title: `Are you sure?`,
          text: `Proceed to checkout?`,
          icon: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: `Yes, let's proceed!`
        }).then((result) => {
          if (result.isConfirmed) {
            fetch(`${process.env.REACT_APP_API_URL}/orders/checkout/`, {
                method: "POST",
                headers: {
                    "Content-Type": "application/json",
                    'Authorization': `Bearer ${localStorage.getItem('myToken')}`
                }
            }).then((result) => result.json())
            .then((data) => {
                console.log(data);
                if(data.isOk === true){
                    Swal.fire({
                      icon: 'success',
                      title: `Successfully Checked Out`,
                      text: data.message,
                    })
                    navigate('/products');
                }else{
                    Swal.fire({
                      icon: 'error',
                      title: 'Oops...',
                      text: data.message,
                    })
                }
            });
          }
        });*/
    }

    useEffect(() => {
        
        /*fetch(`${process.env.REACT_APP_API_URL}/orders/mycart/`, {
                method: "GET",
                headers: {
                    "Content-Type": "application/json",
                    'Authorization': `Bearer ${localStorage.getItem('myToken')}`
                }
            }).then((result) => result.json())
            .then((data) => {
                if(data.isOk != false){
                    let QTYholder = 0, TOTHolder = 0;
                    data.forEach(item => {
                        QTYholder += item.quantity;
                        TOTHolder += item.subtotal;
                    });
                    setTotalQty(QTYholder);
                    setTotalOrder(TOTHolder);

                    setProceed((data.length < 1) ? false : true);

                    setCart(data.map(item => {
                        return (
                            <tr key={item.productId}>
                                <td>
                                    <h5>
                                        {item.name} @ &#8369;{item.price.toFixed(2)}
                                    </h5>
                                    <small>{item.productId}</small>
                                </td>
                                <td className='align-middle text-center'>
                                    <h5>
                                        {item.quantity}
                                    </h5>
                                    <div className='text-center'>
                                        <Button 
                                            className='mx-1'
                                            variant="danger"
                                            onClick={() => {
                                                DeductQuantity(item.productId, item.quantity);
                                            }}
                                        >-</Button>
                                        <Button 
                                            className='mx-1 '
                                            variant="success"
                                            onClick={() => {
                                                AddQuantity(item.productId, item.quantity);
                                            }}
                                        >+</Button>
                                    </div>
                                </td>
                                <td className='align-middle text-end'><h5>&#8369;{item.subtotal.toFixed(2)}</h5></td>
                                <td className='align-middle text-center'>
                                    <Button className="align-middle btn btn-danger" onClick={() => {
                                        toArchive(item.productId);
                                    }}>Remove</Button>
                                </td>    
                            </tr>
                        );
                    }));
                }else{
                    Swal.fire({
                      icon: 'error',
                      title: 'Oops...',
                      text: data.message,
                    })
                    navigate('/');
                }

                
            });*/

    }, [])
    return(
        <>
            <h1 className="text-center my-4">Orders</h1>
            <Table striped bordered hover responsive>
                <thead>
                    <tr className="text-center">
                        <th>Order ID</th>
                        <th>Quantity</th>
                        <th>Total Price</th>
                        <th>Actions</th>
                    </tr>
                </thead>

                <tbody>
                    {
                        (cart.length < 1) ?
                            <tr>
                                <td colSpan='5' className='text-center text-danger'>
                                    <h5 >
                                        No Items on Cart
                                    </h5>
                                </td>
                                   
                            </tr>
                        :
                            cart
                    }
                </tbody>
            </Table>

            {/*<Container>
                <Row>
                    <Col className='offset-md-6 col-6'>
                        <Table>
                            <tbody>
                                <tr>
                                    <td>
                                        <h4>
                                            Total # of Items:
                                        </h4>
                                    </td>
                                    <td className='align-middle text-end'>
                                        <h4>
                                            {totalQty}
                                        </h4>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <h4>
                                            Order Total:
                                        </h4>
                                    </td>
                                    <td className='align-middle text-end'>
                                        <h4>
                                            &#8369;{totalOrder.toFixed(2)}
                                        </h4>
                                    </td>
                                </tr>
                                <tr>
                                    <td colSpan="2"  className="align-middle text-end" >
                                        {
                                            (proceed) ? 
                                                <Button className="btn btn-success" onClick={() => {
                                                    CheckOut();
                                                }}
                                                >PROCEED TO CHECKOUT</Button>
                                            :
                                                <Button className="btn btn-success" onClick={() => {
                                                CheckOut();
                                            }}
                                            disabled>PROCEED TO CHECKOUT</Button>
                                        }
                                    </td>
                                </tr>
                            </tbody>
                        </Table>
                    </Col>
                </Row>
            </Container>*/}
        </>

        )
}