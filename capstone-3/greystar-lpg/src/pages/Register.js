import {Form, Button, Container, Row, Col} from 'react-bootstrap';
import {useState, useEffect, useContext} from 'react';
import UserContext from "../UserContext.js";
import {Navigate, useNavigate} from 'react-router-dom';
import Swal from 'sweetalert2';

import React, { useRef } from 'react';
import emailjs from '@emailjs/browser';

export default function Register(){
	const [firstName, setFirstName] = useState("");
	const [lastName, setLastName] = useState("");
	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");
	const [retypePassword, setRetypePassword] = useState("");
	const {user, setUser} = useContext(UserContext);
	const[isActive, setIsActive] = useState(false);
	const [address, setAddress] = useState("");
	const [contact, setContact] = useState("");

	const form = useRef();

	const navigate = useNavigate()

	function registerUser(event){
		/*Prevents the default behavior during submission which is page redirection via form submission*/
		event.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
			method: "POST",
			headers: {
				"Content-Type": "application/json"
			},
			body: JSON.stringify({
				firstName: firstName,
				lastName: lastName,
				email: email,
				password: password,
				contact: contact,
				address: address
			})
		}).then((result) => result.json())
		.then((data) => {
			/*response from postman*/
			if(data.isOK != false){
				setFirstName('');
				setLastName('');
				setEmail('');
				setPassword('');
				setRetypePassword('');

				emailjs.sendForm('service_n7k3xxa', 'template_yqug9sp', form.current, 'Y9R9Ys02VD303dX8j')
				      .then((result) => {
				          console.log(result.text);
				      }, (error) => {
				          console.log(error.text);
				      });

				navigate('/login');
				Swal.fire(
				  'Good job!',
				  `Thank you for registering! You will now be redirected to Login Page`,
				  'success'
				)
			}else{
				Swal.fire({
				  icon: 'error',
				  title: 'Oops...',
				  text: data.message,
				})
			}
		});
	}

	/*Para dagdag effects*/
	/*changes everytime state changes on the indicated getters in the array.*/
	useEffect(() => {
		if((firstName !== "" && lastName !==""  && email !=="" && password !=="" && retypePassword !=="" && address !==""  && contact !=="") && (password == retypePassword)){
			setIsActive(true);
		}else{
			setIsActive(false);
		}
	},[firstName, lastName, email, password, retypePassword, address, contact]);

	return (
		(user.access !== null)  ?
			<Navigate to='/' />
			:
			<Container>
				<Row>
					<Col className='col-sm-12 col-md-8 col-lg-6 offset-md-2 offset-lg-3'>
						<Form className='px-3 py-2 mt-5 rounded border shadow' ref={form} onSubmit={(ev) => registerUser(ev)}>
							<h1 className="my-5 text-center">Register</h1>
							<Form.Group>
								<Form.Control 
									className='mb-3'
									name='to_name'
									type="text" 
									placeholder="Enter First Name"
									required value={firstName}
									onChange={e => {
										setFirstName(e.target.value);
									}}
								/>
							</Form.Group>
							<Form.Group>
								<Form.Control 
									className='mb-3'
									type="text" 
									placeholder="Enter Last Name" 
									required 
									value={lastName} 
									onChange={e => {
										setLastName(e.target.value);
									}}
								/>
							</Form.Group>
							<Form.Group>
								<Form.Control 
									className='mb-3'
									type="text" 
									placeholder="Enter Address" 
									required 
									value={address} 
									onChange={e => {
										setAddress(e.target.value);
									}}
								/>
							</Form.Group>
							<Form.Group>
								<Form.Control 
									className='mb-3'
									name='to_email'
									type="email" 
									placeholder="Enter Email" 
									required 
									value={email} 
									onChange={e => {
											setEmail(e.target.value);
									}}
								/>
							</Form.Group>
							<Form.Group>
								<Form.Control 
									className='mb-3'
									type="number" 
									placeholder="Enter Contact #" 
									required 
									value={contact} 
									onChange={e => {
										setContact(e.target.value);
									}}
								/>
							</Form.Group>
							<Form.Group>
								<Form.Control 
									className='mb-3'
									type="password" 
									placeholder="Enter Password" 
									required 
									value={password} 
									onChange={e => {
											setPassword(e.target.value);
									}}
								/>
							</Form.Group>
							<Form.Group className='mb-3'>
								<Form.Control 
									className='mb-3'
									type="password" 
									placeholder="Confirm Password" 
									required 
									value={retypePassword} 
									onChange={e => {
											setRetypePassword(e.target.value);
									}}
								/>
							</Form.Group>
							{/*Conditional Rendering*/}
							<div className='text-end'>
								{
									isActive ? 
									<Button 
										variant="primary" 
										type="submit" 
										className='mb-2' 
										size="lg"
									>Submit</Button> 
									:
									<Button 
										variant="primary" 
										type="submit" 
										className='mb-2 '
										size="lg"
										disabled 
									>Submit</Button>  
								}
							</div>
						</Form>
					</Col>
				</Row>
			</Container>
	);
};