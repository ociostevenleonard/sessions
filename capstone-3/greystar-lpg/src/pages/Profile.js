import {Row, Col, Container, Table} from 'react-bootstrap';
import {useState, useContext, useEffect} from 'react';
import UserContext from "../UserContext.js";
import {Navigate} from 'react-router-dom';
import {Link} from 'react-router-dom'

import MyOrders from '../components/MyOrders.js';
import ResetPassword from '../components/ResetPassword.js';
import UpdateInfo from '../components/UpdateInfo.js';
import BeatLoader from "react-spinners/BeatLoader";

export default function Profile(){		
	const {user} = useContext(UserContext);

	const [fName, setFName] = useState("");
	const [LName, setLName] = useState("");
	const [email, setEmail] = useState("");
	const [isAdmin, setIsAdmin] = useState("");
	const [contact, setContact] = useState("");
	const [address, setAddress] = useState("");

    const [products, setProducts] = useState([])

  	const [loading, setLoading] = useState(false);

	useEffect(()=>{
		setLoading(true);
		fetch(`${process.env.REACT_APP_API_URL}/users/viewDetails/`, {
			method: "GET",
			headers: {
				"Content-Type": "application/json",
				'Authorization': `Bearer ${localStorage.getItem('myToken')}`
			}
		}).then((result) => result.json())
		.then((data) => {
			setLoading(false);
			setFName(data.firstName);
			setLName(data.lastName);
			setEmail(data.email);
			setAddress(data.address);
			setContact(data.contact);
			if(data.isAdmin){
				setIsAdmin("(Admin)");
			}
			if(!data.isAdmin){
				fetch(`${process.env.REACT_APP_API_URL}/orders/viewOrders/`, {
				        method: "GET",
				        headers: {
				            "Content-Type": "application/json",
				            'Authorization': `Bearer ${localStorage.getItem('myToken')}`
				        }
				    }).then((result) => result.json())
				    .then((data) => {
				        setProducts(data);
				    });
			}
		});
	}, [fName, LName, email, contact, address])


	function showProfile(){
		return (
			<Row className='forHome p-3' >
				{
					loading ?
						<div className='text-center p-5'>
						  	<BeatLoader loading={loading} size={12} aria-label="Loading Spinner" data-testid="loader" />  
						</div>
					:

						<Col className='p-5'>
							<Container className='rounded border p-5 bg-white shadow' fluid>

								<h3>
									Profile
								</h3>
								<h4 className='pt-5 pb-3'>
									{fName} {LName} {isAdmin}
								</h4>
									<div>
										<ResetPassword user={email} />
										<UpdateInfo />
									</div>
								<hr />
								<h4>
									Address
								</h4>
								<ul>
									<li>{address}</li>
								</ul>
								<hr />
								<h4>
									Contacts
								</h4>
								<ul>
									<li>Email: {email}</li>
									<li>Contact #: {contact}</li>
								</ul>
							</Container>
						</Col>
				}
			</Row>
		);
	}

	function showMyOrders(){
		return (
			(isAdmin != "(Admin)") ?
				<Row>
					<Col className='p-5'>
						<Container className='rounded border p-3 shadow' fluid>
							<h3>
								My Orders
							</h3>
							<hr />
							<Table striped bordered hover responsive>
							    <thead>
							        <tr className="text-center">
							            <th>Order ID</th>
							            <th>Recent Status</th>
							            <th>Last Update</th>
							            <th>Remarks</th>
							        </tr>
							    </thead>
							    <tbody>
							    	{
							    		products.toReversed().map(item => {
					    		            return (
					    		            	<MyOrders data_from_user_orders={item}/>						    		                
					    		            );
					    		        })
							    	}
							        
							    </tbody>
							</Table>
						</Container>
					</Col>
				</Row>
			:
			<h3>
				
			</h3>
		);
	}

	return (
			(user.access === null)  ?
				<Navigate to='/'/>
			:
				<>
					{showProfile()}
					{showMyOrders()}
				</>
	);
}