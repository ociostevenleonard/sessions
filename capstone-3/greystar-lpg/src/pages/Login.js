import {Form, Button, Container, Row, Col, Image} from 'react-bootstrap';
import {useState, useEffect, useContext} from 'react';
import UserContext from "../UserContext.js";
import {Navigate, Link} from 'react-router-dom';
import Swal from 'sweetalert2';
import BeatLoader from "react-spinners/BeatLoader";
import Footer from '../components/Footer.js';

export default function Login(){
	/*Get  Global Variables*/
	const {user, setUser} = useContext(UserContext);

	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");

	const [isActive, setIsActive] = useState(false);

  	const [loading, setLoading] = useState(false);


	function login(event){
		event.preventDefault();

		setIsActive(false);
		setLoading(true);

		fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
			method: "POST",
			headers: {
				"Content-Type": "application/json"
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		}).then((result) => result.json())
		.then((data) => {
			setLoading(false);
			if(data.isLogin === false){
				Swal.fire({
				  icon: 'error',
				  title: 'Oops...',
				  text: data.message,
				})
			}else{
				setEmail('');
				setPassword('');
				localStorage.setItem('myToken', data.myToken);
				setUser({
					access: localStorage.getItem('myToken')
				})
				Swal.fire(
				  'Good job!',
				  `${email} Successfully logged-in!`,
				  'success'
				)
			}
		});
	}
	useEffect(() => {
		console.log(`${process.env.REACT_APP_API_URL} -asd--->`);
		if(email !=="" && password !==""){
			setIsActive(true);
		}else{
			setIsActive(false);
		}
	},[email, password]);
	return (
			(user.access !== null) ?
				<Navigate to='/' />
			:
				<>
					<Container>
						<Row className='py-5 my-5'>
							<Col className='col-lg-4 offset-lg-2 col-md-6 col-sm-12 col-xs-12'>
								<Image src={require('../images/system/greystar-logo.png')} className='w-100'/>
							</Col>


							<Col className='col-lg-4 col-md-6 col-sm-12 col-xs-12'>
								<Container fluid className='shadow rounded border p-3'>
									<Form onSubmit={(ev) => login(ev)}>
										<h3 className="mt-3">Login</h3>
										<Form.Group>
											<Form.Label>Email:</Form.Label>
											<Form.Control 
												type="email" 
												placeholder="Enter Email" 
												required 
												value={email} 
												onChange={e => {
														setEmail(e.target.value);
												}}
											/>
										</Form.Group>
										<Form.Group className='mb-3'>
											<Form.Label>Password:</Form.Label>
											<Form.Control 
												type="password" 
												placeholder="Enter Password" 
												required 
												value={password} 
												onChange={e => {
														setPassword(e.target.value);
												}}
											/>
										</Form.Group>
										<div className='text-end'>
											{
												isActive ? 
												<Button 
													variant="success" 
													type="submit" 
													className='mb-2' 
												>LOGIN</Button> 
												:
												<Button 
													variant="success" 
													type="submit" 
													className='mb-2'
													disabled 
												>LOGIN</Button>  
											}

											<div className='text-center'>
												<BeatLoader loading={loading} size={12} aria-label="Loading Spinner" data-testid="loader" />	
											</div>
										</div>		
									</Form>
									<div className='text-center'>
										No account yet? <Link to={'/register'} className='LinkDecor'>Sign-up Here</Link>
									</div>
								</Container>
							</Col>
						</Row>
					</Container>
					<Footer />
				</>
	);
};