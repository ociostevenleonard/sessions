import {Row, Col, Card, Button} from 'react-bootstrap';
import Swal from 'sweetalert2';
import {useEffect, useState, useContext} from 'react';
import AdminView from '../components/AdminView';
import UserView from '../components/UserView';
import UserContext from '../UserContext.js'

export default function Products(){	
	const {user} = useContext(UserContext);
	const [isAdmin, setIsAdmin] = useState(false);

	/*Check if Admin*/
	if(user.access !== null){
		fetch(`${process.env.REACT_APP_API_URL}/users/viewDetails/`, {
			method: "GET",
			headers: {
				"Content-Type": "application/json",
				'Authorization': `Bearer ${localStorage.getItem('myToken')}`
			}
		}).then((result) => result.json())
		.then((data) => {
			setIsAdmin(data.isAdmin);
		});
	}

	let URL = (isAdmin) ? `${process.env.REACT_APP_API_URL}/products/showAll/` : `${process.env.REACT_APP_API_URL}/products/showallactive/`;
	let headers =  (isAdmin) ? {
		"Content-Type": "application/json",
		'Authorization': `Bearer ${localStorage.getItem('myToken')}`
	} : {
		"Content-Type": "application/json"
	}


	const [products, setProdcuts] = useState([]);
	
	return (
			(isAdmin) ?
				<AdminView productData={products}/>
			:
			<>
				<UserView productData={products} />
			</>	
	);
}