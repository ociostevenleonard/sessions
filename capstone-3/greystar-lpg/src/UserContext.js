import React from 'react';

/*Creates a context object for sharing data between components*/
const UserContext = React.createContext();

/*UserProvider allows to share data to UserContext, for components/pages*/
export const UserProvider = UserContext.Provider;

export default UserContext;